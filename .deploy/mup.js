// docker start --interactive  [NAME]
module.exports = {
    servers: {
        one: {
            host: 'freewill.org',
            username: 'root',
            // pem: './path/to/pem'
            password: 'w4H,CP+[jh2.}X2m'
            // or neither for authenticate from ssh-agent
        }
    },

    app: {
        name: 'freewill',
        path: '../',

        servers: {
            one: {},
        },

        buildOptions: {
            serverOnly: true,
            debug: true,
        },

        env: {
            // TODO: Change to your app's url
            // If you are using ssl, it needs to start with https://
            ROOT_URL: 'http://freewill.org',
            MONGO_URL: 'mongodb://localhost:3001/freewill',
        },

        // ssl: { // (optional)
        //   // Enables let's encrypt (optional)
        //   autogenerate: {
        //     email: 'email.address@domain.com',
        //     // comma separated list of domains
        //     domains: 'website.com,www.website.com'
        //   }
        // },

        docker: {
            // change to 'kadirahq/meteord' if your app is using Meteor 1.3 or older
            image: 'abernix/meteord:base',
        },

        // Show progress bar while uploading bundle to server
        // You might need to disable it on CI servers
        enableUploadProgressBar: true
    },

    mongo: {
        version: '3.4.1',
        servers: {
            one: {}
        }
    }
};
