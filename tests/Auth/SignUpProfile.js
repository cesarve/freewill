/**
 * Created by cesar on 12/9/17.
 */

import faker from 'faker'


import {cities} from './cities'
const fields = ['profile.firstName', 'profile.familyName', 'profile.email', 'profile.password', 'terms']
const errors = ['Please enter a location', 'Please enter a phone number', 'Please enter a your birthday']


const faked = {
    firstName: faker.name.firstName(),
    familyName: faker.name.lastName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
    description: faker.lorem.sentence(),
    phonePrefix: ['ve', 'us', 'au', 'es', 'ec', 'ar', 'br'][Math.floor(Math.random() * 7)],
    phoneNumber: faker.phone.phoneNumber(),
    location: cities[Math.floor(Math.random() * cities.length)],
    birthday: '1976-01-30T10:30:00',
    gender: ['Male', 'Female'][Math.floor(Math.random() * 2)]

}
let locationValue


describe('Auth Sign up profile, step 2 (create a profile)  @watch', function () {
    it('check all form validators ', function () {
        browser.url('http://localhost:3000')
        browser.click('[href="/sign-up"]')
        if (!browser.isExisting('.ui.form.loading')){
            browser.waitForExist('.ui.form.loading', 1000);
            browser.waitForExist('.ui.form.loading', 3000, true);
        }


        browser.waitUntil(() => browser.isEnabled('form input[type="submit"]'), 4000);
        browser.click('form input[type="submit"]')
        errors.forEach((errorMsg, i) => {
            expect(browser.$('.ui.error.message').getText()).to.have.string(errorMsg)
            expect(browser.$$('.ui.red.pointing.basic.label')[i].getText()).to.have.string(errorMsg)
        })
    })
    it('check clean  validator ', function () {
        browser.setValue('form input[name="profile.firstName"]', faked.firstName)
        browser.setValue('form input[name="profile.familyName"]', faked.familyName)
        browser.setValue('form textarea[name="profile.description"]', faked.description)
        browser.setValue('form div[name="profile.location"] input', faked.location)


        browser.waitForExist('form div[name="profile.location"] div[role="option"]',2500)
        browser.click('form div[name="profile.location"] div[role="option"]')

        //locationValue=browser.getValue('form div[name="profile.location"]')
        //console.log('locationValue',locationValue)
        expect(browser.$('.ui.error.message').getText()).to.not.have.string(errors[0])

        browser.setValue('form input[name="profile.phone.number"]', faked.phoneNumber)
        expect(browser.$('.ui.error.message').getText()).to.not.have.string(errors[1])


        browser.middleClick('form input[name="profile.birthday"]');
        browser.keys('30011976\t1030a')
        expect(browser.isExisting('.ui.error.message')).to.be.equal(false)

        browser.click('form div[name="profile.gender"]')
        browser.click('form div[name="profile.gender"] [role="option"]')

    })
    it('check for update users details ', function () {
        browser.click('form input[type="submit"]')
        const user = browser.execute(() => Meteor.user()).value
        console.log(user)
        assert(user.profile.firstName === faked.firstName)
        assert(user.profile.lastName === faked.lastName)
        assert(user.profile.description === faked.description)
        assert(user.profile.phone.number === faked.phoneNumber)
        //assert(user.profile.birthday === faked.birthday)
        //todo check for other fields values were changed in database
    })
})



