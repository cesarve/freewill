/**
 * Created by cesar on 12/9/17.
 */
/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */
// These are Chimp globals
/* globals browser assert server */
import faker from 'faker'
/*
 beforeEach(function () {
 browser.url('http://localhost:3000')
 server.call('generateFixtures')
 })
 */
const fields = ['profile.firstName', 'profile.familyName', 'profile.email', 'profile.password', 'terms']
const errors = ['Please enter your name', 'Please enter your family name', 'Please enter your email', 'Please enter a password', 'You should accept the terms']
const faked = {
    firstName: faker.name.firstName(),
    familyName: faker.name.lastName(),
    email: faker.internet.email(),
    password: faker.internet.password(),
}
describe('Auth Sign up, step 1 (create an account) ', function () {
    it('check all form validators  ', function () {
        browser.url('http://localhost:3000')
        browser.execute(() => Meteor.logout())
        browser.click('[href="/sign-up"]')
        browser.click('form input[type="submit"]')

        errors.forEach((errorMsg, i) => {
            expect(browser.$('.ui.error.message').getText()).to.have.string(errorMsg)
            expect(browser.$$('.ui.red.pointing.basic.label')[i].getText()).to.have.string(errorMsg)
        })
    })
    it('check clean  validator ', function () {
        browser.setValue('form input[name="profile.firstName"]', faked.firstName)
        expect(browser.$('.ui.error.message').getText()).to.not.have.string(errors[0])
        browser.setValue('form input[name="profile.familyName"]', faked.familyName)
        expect(browser.$('.ui.error.message').getText()).to.not.have.string(errors[1])
        browser.setValue('form input[name="email"]', faked.email)
        expect(browser.$('.ui.error.message').getText()).to.not.have.string(errors[2])
        browser.setValue('form input[name="password"]', faked.password)
        expect(browser.$('.ui.error.message').getText()).to.not.have.string(errors[3])
        browser.click('label[for="uniforms-0000-0005"]')
        expect(browser.isExisting('.ui.error.message')).to.be.equal(false)
    })

    it('check new users was created ', function () {
        browser.click('form input[type="submit"]')
        browser.waitUntil(()=>browser.getText('.active.step div.title')==='Create Profile',2000)
        const user=browser.execute(function () {
            return Meteor.user()
        }).value
        assert(user.emails[0].address === faked.email)
        assert(user.profile.firstName === faked.firstName)
        assert(user.profile.lastName === faked.lastName)

    })
})
