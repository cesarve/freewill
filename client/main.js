import {Meteor} from 'meteor/meteor';
import {render} from 'react-dom';
//import '/imports/startup/client'
import React from 'react';
import App from '/imports/ui/App';


Meteor.startup(() => {


    const renderStart = Date.now()
    const startupTime = renderStart - window.performance.timing.responseStart
    console.log('Meteor start took ', startupTime)


    renderAsync().then(() => {
        const renderTime = Date.now() - renderStart
        console.log('renderAsync took ', renderTime)
        console.log('total time', startupTime + renderTime)
    })
    async function renderAsync() {
        /** todo check the performace if we import the following
         const {'default': React} = await import ('react');
         const {render} = await import ('react-dom');
         const {'default': ApolloClient} = await import ('apollo-client');
         const {meteorClientConfig} = await import ('meteor/apollo');
         const {ApolloProvider} = await import ('react-apollo');
         const {'default': App} = await import ('/imports/ui/App');
         */
        render(
            <App />,
            document.getElementById('app')
        )
    }


});

