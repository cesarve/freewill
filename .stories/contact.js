/**
 * Created by cesar on 22/5/17.
 */
import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container, Grid} from 'semantic-ui-react'
import Contact from '/imports/ui/components/Home/Contact';

import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

import localeData from './../build/locales/data.json';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = localeData[language] || localeData.en;

storiesOf('Contact', module)

    .add('SignUp', () => (
        <Container>
        	<IntlProvider locale={navigator.language} messages={messages}>	
	            <Contact >

	            </Contact>
	        </IntlProvider>
        </Container >
    ))
