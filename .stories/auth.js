/**
 * Created by cesar on 22/5/17.
 */
import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import SignIn from '/imports/ui/components/Auth/SignIn';
import SignUp from '/imports/ui/components/Auth/SignUp';
import SignUpTerms from '/imports/ui/components/Auth/SignUpTerms';
import {Container, Grid} from 'semantic-ui-react'
import SignUpSteps from '/imports/ui/components/Auth/SignUpSteps';
import SignUpProfile from "../imports/ui/components/Auth/SignUpProfile";
import SignUpVerify from "../imports/ui/components/Auth/SignUpVerify";
storiesOf('Auth', module)
    .add('SignIn', () => (
        <Grid centered>
            <Grid.Column centered mobile={12} tablet={8} computer={6}>
                <SignIn />
            </Grid.Column>
        </Grid>
    ))
    .add('SignUp', () => (
        <Container>
        <SignUp  />
        </Container >
    ))

    .add('Terms', () => (
        <Container>
            <SignUpTerms />
        </Container>
    ))

    .add('Steps', () => (
        <Container>
            <SignUpSteps />
        </Container>
    ))

    .add('Profile', () => (
        <Container>
            <SignUpProfile />
        </Container>
    ))


    .add('Verify', () => (
        <Container>
            <SignUpVerify />
        </Container>
    ))

