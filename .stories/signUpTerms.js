import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import SignUpTerm from '/imports/ui/components/Auth/SignUpTerms';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import localeData from './../build/locales/data.json';

addLocaleData([...en, ...es]);
const language = navigator.languages;
const messages = localeData[language] || localeData.en;

storiesOf('SignUpTerm', module)
    .add('SignUpTerm New', () => (
        <Container>
			<IntlProvider locale={navigator.language} messages={messages}>	
            	<SignUpTerm />
			</IntlProvider>
        </Container >
    ))

