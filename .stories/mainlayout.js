/**
 * Created by cesar on 16/5/17.
 */
import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Image} from 'semantic-ui-react'
import MainLayout from '/imports/ui/components/Layouts/MainLayout';


storiesOf('Main Layout', module)
    .add('Main Layout', () => (
        <MainLayout>
            <Image src='https://react.semantic-ui.com/assets/images/wireframe/paragraph.png'/>
            <br/>
            <Image src='https://react.semantic-ui.com/assets/images/wireframe/paragraph.png'/>
            <br/>
            <Image src='https://react.semantic-ui.com/assets/images/wireframe/paragraph.png'/>
            <br/>
            <Image src='https://react.semantic-ui.com/assets/images/wireframe/paragraph.png'/>
            <br/>
            <Image src='https://react.semantic-ui.com/assets/images/wireframe/paragraph.png'/>
            <br/>
            <Image src='https://react.semantic-ui.com/assets/images/wireframe/paragraph.png'/>
            <br/>
            <Image src='https://react.semantic-ui.com/assets/images/wireframe/paragraph.png'/>
        </MainLayout>
    ))


