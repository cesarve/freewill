import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import SignUpVerify from '/imports/ui/components/Auth/SignUpVerify';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import localeData from './../build/locales/data.json';

addLocaleData([...en, ...es]);
const language = navigator.languages;
const messages = localeData[language] || localeData.en;

storiesOf('SignUpVerify', module)
    .add('SignUpVerify New', () => (
        <Container>
			<IntlProvider locale={navigator.language} messages={messages}>	
            	<SignUpVerify />
			</IntlProvider>
        </Container >
    ))

