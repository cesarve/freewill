import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import Products from '/imports/ui/components/Provision/Products/Products';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import localeData from './../build/locales/data.json';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = localeData[language] || localeData.en;

storiesOf('Products', module)
    .add('Products New', () => (
        <Container>	
        	<IntlProvider locale={navigator.language} messages={messages}>	
            	<Products />
            </IntlProvider>
        </Container >
    ))
