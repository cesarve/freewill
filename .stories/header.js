import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Header} from '/imports/ui/components/Header/Header';
/*
 import SearchType from '/imports/ui/components/Header/SearchType';
 import SearchInput from '/imports/ui/components/Header/SearchInput';
 */
import {addLocaleData, IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';


addLocaleData([...en, ...es]);

const language = navigator.languages;


export const optionsType = [
    {
        text: 'Creators',
        value: 'Creators',
        icon: 'user',
    },
    {
        text: 'Abilities',
        value: 'Abilities',
        icon: 'wait',
    },
    {
        text: 'Products',
        value: 'Products',
        icon: 'gift',
    },
    {
        text: 'Organizations',
        value: 'Organizations',
        icon: 'users',
    },
]

export const optionsResult = [
    {
        text: 'Gold Coast (5)',
        value: ' Gold Coast',
    },

]
storiesOf('Header', module)
    .add('Header', () => (

        <IntlProvider locale={navigator.language}>
            <div>
                <Header  />

                <p>1</p>
                <p>2</p>
                <p>3</p>
                <p>4</p>
                <p>5</p>
                <p>6</p>
                <p>7</p>
                <p>8</p>
                <p>9</p>
                <p>0</p>

                <p>1</p>
                <p>2</p>
                <p>3</p>
                <p>4</p>
                <p>5</p>
                <p>6</p>
                <p>7</p>
                <p>8</p>
                <p>9</p>
                <p>0</p>


                <p>1</p>
                <p>2</p>
                <p>3</p>
                <p>4</p>
                <p>5</p>
                <p>6</p>
                <p>7</p>
                <p>8</p>
                <p>9</p>
                <p>0</p>


                <p>1</p>
                <p>2</p>
                <p>3</p>
                <p>4</p>
                <p>5</p>
                <p>6</p>
                <p>7</p>
                <p>8</p>
                <p>9</p>
                <p>0</p>
            </div>
        </IntlProvider>
    ))
/*
 .add('SearchType', () => (
 <SearchType options={optionsType} />
 ))
 .add('SearchInput', () => (
 <SearchInput options={optionsResult} />
 ))
 */


