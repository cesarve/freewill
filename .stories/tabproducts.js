import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import TabsProducts from '/imports/ui/components/Provision/Products/TabsProducts';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = {}

storiesOf('TabsProducts', module)
    .add('TabsProducts New', () => (
        <Container>	
        	<IntlProvider locale={navigator.language} messages={messages}>	
            	<TabsProducts />
            </IntlProvider>
        </Container >
    ))
