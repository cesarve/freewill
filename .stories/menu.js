import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import Menu from '/imports/ui/components/Admin/Menu/Menu';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = {}

storiesOf('Menu', module)
    .add('Menu New', () => (
        <Container>	
        	<IntlProvider locale={navigator.language} messages={messages}>	
            	<Menu />
            </IntlProvider>
        </Container >
    ))
