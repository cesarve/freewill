import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import LenguajeTab from '/imports/ui/components/Admin/Lenguaje_Tab/LenguajeTab';



storiesOf('LenguajeTab', module)
    .add('LenguajeTab New', () => (
        <Container>		
            	<LenguajeTab />
        </Container >
    ))
