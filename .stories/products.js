import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import Product from '/imports/ui/components/Products/Product';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import localeData from './../build/locales/data.json';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = localeData[language] || localeData.en;

console.log('Product',Product)

storiesOf('Products', module)
    .add('Product New', () => (
        <Container>
			<IntlProvider locale={navigator.language} messages={messages}>	
            	<Product />
			</IntlProvider>
        </Container >
    ))
