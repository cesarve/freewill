/**
 * Created by cesar on 4/9/17.
 */
import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import SimpleSchema  from "simpl-schema";
import DatePicker from "../imports/ui/uniforms/DatePicker";


const bridge = new SimpleSchema({
    test: String
})


storiesOf('Uniforms', module)
    .add('DatePicker', () => (
        <DatePicker/>
    ))


