import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import Plan from '/imports/ui/components/Static/Plan';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = {}

storiesOf('Plan', module)
    .add('Plan New', () => (
        <Container>	
        	<IntlProvider locale={navigator.language} messages={messages}>	
            	<Plan />
            </IntlProvider>
        </Container >
    ))
