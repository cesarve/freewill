/**
 * Created by Jose carmona on 02/10/17.
 */

import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'


// Our translated strings
import localeData from './data.json';

import {IntlProvider, addLocaleData, FormattedMessage} from 'react-intl'
import en from 'react-intl/locale-data/en'
import es from 'react-intl/locale-data/es'
import fr from 'react-intl/locale-data/fr'
import it from 'react-intl/locale-data/it'


addLocaleData([...en, ...es, ...fr, ...it]);

// Define user's language. Different browsers have the user locale defined
// on different fields on the `navigator` object, so we make sure to account
// for these different by checking all of them
const language = (navigator.languages && navigator.languages[0]) ||
    navigator.language ||
    navigator.userLanguage;


// Split locales with a region code
const languageWithoutRegionCode = language.toLowerCase().split(/[_-]+/)[0];

// Try full locale, try locale without region code, fallback to 'en'
const messages = localeData[languageWithoutRegionCode] || localeData[language] || localeData.en;

storiesOf('TestProductFather', module)
    .add('TestProductFather New', () => (
        <IntlProvider locale={language} messages={messages}>
            <FormattedMessage
                id={ 'hello001' }
                defaultMessage={ 'Hello, {name}!' }
                values={{ name: 'cesar' }}
            />
        </IntlProvider>
    ))
