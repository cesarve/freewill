import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import TabsAbilities from '/imports/ui/components/Provision/Abilities/TabsAbilities';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = {}

storiesOf('TabsAbilities', module)
    .add('TabsAbilities New', () => (
        <Container>	
        	<IntlProvider locale={navigator.language} messages={messages}>	
            	<TabsAbilities />
            </IntlProvider>
        </Container >
    ))
