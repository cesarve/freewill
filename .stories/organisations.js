import React, {Component, PropTypes} from 'react';
import {storiesOf} from '@storybook/react';
import {Container} from 'semantic-ui-react'
import Organisations from '/imports/ui/components/Provision/Organisations/Organisations';

//React intl
import { addLocaleData , IntlProvider} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = {}

storiesOf('Organisations', module)
    .add('Organisations New', () => (
        <Container>	
        	<IntlProvider locale={navigator.language} messages={messages}>	
            	<Organisations />
            </IntlProvider>
        </Container >
    ))
