import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connectField} from "uniforms";
import AvatarEditor from 'react-avatar-editor'
import {Button} from "semantic-ui-react";
import './avatar-field.css'
class AvatarField extends Component {
    constructor(props) {
        super(props)
        this.state = {scale: 100, image: props.image}
    }

    componentDidMount() {
        this.container.addEventListener("mousewheel", this.onWheel, false);
        this.container.addEventListener("DOMMouseScroll", this.onWheel, false);  // Firefox
    }

    componentWillUnmount() {
        this.container.removeEventListener("mousewheel", this.onWheel, false);
        this.container.removeEventListener("DOMMouseScroll", this.onWheel, false);  // Firefox
    }

    onWheel = (e = window.event) => {
        const delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
        let scale = this.state.scale
        scale += delta
        if (scale < 100) scale = 100
        if (scale > 300) scale = 300
        this.setState({scale})
        e.preventDefault()
        e.stopPropagation()

    }

    launchFilePicker = (e) => {
        e.preventDefault()
        this.fileInput.dispatchEvent(new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
        }));
    }
    loadFile = () => {
        const image = this.fileInput.files[0];
        this.setState({image, scale: 100})
    }

    onImageChange = () => {
        if (!this.state.image) return
        const {onChange, mime} = this.props
        const canvasScaled = this.editor.getImageScaledToCanvas()
        const base64 = canvasScaled.toDataURL(mime);
        onChange(base64)

    }
    setEditorRef = (editor) => {
        this.editor = editor
    }

    render() {
        const {id, name, defaultAvatar, width, height, border, color, borderRadius, value} = this.props
        const {scale, image} = this.state
        return (
            <div className="avatar-container">
                <div className="avatar-field" style={{
                    background: `${!image ? `url(${value || defaultAvatar})` : '#fff'}`,
                    width: `${width}px`,
                    height: `${height}px`,
                }}
                     onClick={(e) => !image && this.launchFilePicker(e)}
                     ref={(container) => {
                         this.container = container
                     }}>
                    <Button icon="edit"
                            onClick={this.launchFilePicker}
                            style={{display: !image ? 'none' : 'block'}}/>
                    <AvatarEditor
                        ref={this.setEditorRef}
                        image={image}
                        width={width}
                        height={height}
                        border={border}
                        color={color} // RGBA
                        scale={scale / 100}
                        //rotate={0}
                        borderRadius={borderRadius}
                        onImageChange={this.onImageChange}
                    />
                </div>
                <input
                    id={id}
                    type='file'
                    name={name}
                    onChange={this.loadFile}
                    ref={(fileInput) => {
                        this.fileInput = fileInput
                    }}
                />
            </div>
        )
    }

    static defaultProps = {
        defaultAvatar: '/img/default-avatar.png',
        width: 200,
        height: 200,
        border: 0,
        color: [255, 255, 255, 0.9],
        borderRadius: 100,
        mime: "image/jpeg",
    }

    static propTypes = {}
}


export default connectField(AvatarField)
