import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connectField, filterDOMProps} from "uniforms";
import calendar from 'semantic-ui-calendar'

class DatePicker extends Component {
    constructor(props){
        super(props)
    }
    componentDidMount(){
        console.log(calendar)
        const {calendarOption}=this.props
        $(`#${id}`).calendar(calendarOption);
    }
    render() {
        const props=filterDOMProps(this.props)
        const {inputOptions}=this.props
        return (
            <input type="text"  {...inputOptions} {...props} />
        )
    }
    
    static defaultProps={
    
    }

    static propTypes={
    
    }
}


export default connectField(DatePicker)
