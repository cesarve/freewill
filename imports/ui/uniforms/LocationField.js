import React from 'react';
import connectField from 'uniforms/connectField';
import { Label}  from 'semantic-ui-react'
import filterSemanticProps from 'uniforms-react-semantic/filterSemanticProps'
import Location from "../semantic-components/Location";



    const LocationField = ({
                             additionLabel,
                             additionPosition,
                             allowAdditions,
                             as,
                             basic,
                             button,
                             children,
                             className,
                             closeOnBlur,
                             closeOnChange,
                             compact,
                             defaultOpen,
                             defaultSelectedLabel,
                             defaultValue,
                             disabled,
                             error,
                             floating,
                             fluid,
                             header,
                             icon,
                             inline,
                             item,
                             labeled,
                             loading,
                             minCharacters,
                             multiple,
                             noResultsMessage,
                             onAddItem,
                             onBlur,
                             onChange,
                             onClick,
                             onClose,
                             onFocus,
                             onLabelClick,
                             onMouseDown,
                             onOpen,
                             onSearchChange,
                             open,
                             openOnFocus,
                             options,
                             placeholder,
                             pointing,
                             renderLabel,
                             scrolling,
                             search,
                             searchInput,
                             selectOnBlur,
                             selectedLabel,
                             selection,
                             simple,
                             tabIndex,
                             text,
                             trigger,
                             upward,

                             errorMessage,
                             showInlineError,

                             ...props,
                         }) => {
    const propsToSemantic = filterSemanticProps(props)
    const location = <Location
        {...propsToSemantic}

        additionLabel={additionLabel}
        additionPosition={additionPosition}
        allowAdditions={allowAdditions}
        as={as}
        basic={basic}
        button={button}
        children={children}
        className={className}
        closeOnBlur={closeOnBlur}
        closeOnChange={closeOnChange}
        compact={compact}
        defaultOpen={defaultOpen}
        defaultSelectedLabel={defaultSelectedLabel}
        defaultValue={defaultValue}
        disabled={disabled}
        error={!!error}
        floating={floating}
        fluid={fluid}
        header={header}
        icon={icon}
        inline={inline}
        item={item}
        labeled={labeled}
        loading={loading}
        minCharacters={minCharacters}
        multiple={multiple}
        noResultsMessage={noResultsMessage}
        onAddItem={onAddItem}
        onBlur={onBlur}
        onClick={onClick}
        onClose={onClose}
        onFocus={onFocus}
        onLabelClick={onLabelClick}
        onMouseDown={onMouseDown}
        onOpen={onOpen}
        onSearchChange={onSearchChange}
        open={open}
        openOnFocus={openOnFocus}
        options={options}
        placeholder={placeholder}
        pointing={pointing}
        renderLabel={renderLabel}
        scrolling={scrolling}
        search={search}
        searchInput={searchInput}
        selectOnBlur={selectOnBlur}
        selectedLabel={selectedLabel}
        selection={selection}
        simple={simple}
        tabIndex={tabIndex}
        text={text}
        trigger={trigger}
        upward={upward}

        onChange={(event, value) => {
            onChange(value)
        }}

    />
    if (!showInlineError) return location
    return (
        <div>
            {location}
            {!!(error && showInlineError) && (
                <Label basic color='red' pointing> {errorMessage}</Label>
            )}
        </div>
    )
}


export default connectField(LocationField, {
    includeInChain: false,
});

