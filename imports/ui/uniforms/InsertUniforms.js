/**
 * Created by cesar on 25/9/17.
 */
import React, {Component} from 'react';
import get         from 'lodash/get';
import BaseForm from 'uniforms/BaseForm';


export default class extends Component {
    static contextTypes = BaseForm.childContextTypes;
    getFunctionFromContext = () => ({
        schema: this.context.uniforms.schema,
        findError: (name) => this.context.uniforms.schema.getError(name, this.context.uniforms.error),
        findField: (name) => this.context.uniforms.schema.getField(name),
        findValue: (name) => get(this.context.uniforms.model, name),
        id: () => this.context.uniforms.randomId(),
        fields: () => this.context.schema.getSubfields(...arguments)
    })
}
