import React, {Component} from 'react';

import {connectField} from 'uniforms';
import {Button, Image, Label, Progress, Reveal} from "semantic-ui-react";
import './upload-field.css'

class FieldUpload extends Component {
    constructor(props) {
        super(props);
        this.state = {uploading: false, progress: 0};


    }

    componentDidMount() {
        this.props.onChange(this.props.value)
    }

    uploadFile = () => {
        try {
            this.setState({error: ''})
            const uploadFile = this.refs.fileInput.files[0];
            const uploader = this.props.slingshot.send(uploadFile, (error, url) => {
                if (error) {
                    console.error(error)
                    this.setState({progress: 0});
                    throw new Meteor.Error('upload-file-fail', error);
                } else {
                    const file = {
                        url,
                        name: uploadFile.name,
                        //size: uploadFile.size,
                        //type: uploadFile.type,
                    };
                    this.setState({progress: 100});
                    this.props.onChange(file);

                }
                this.setState({uploading: false});
            });
            this.setState({
                uploading: true,
            });
            this.intervalId = Meteor.setInterval(() => {
                const progress = uploader.progress()
                this.setState({
                    progress: Math.round(isNaN(progress) ? 0 : progress * 100),
                });
            }, 250)
        } catch (e) {
            this.cancel()
            this.setState({error: e.message})
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextState.uploading === false) {
            Meteor.clearInterval(this.intervalId)
        }
    }

    handleCancelClick = () => {
        this.cancel()
    }
    cancel = () => {
        console.log('calcel')
        this.refs.fileInput.value = ''
        Meteor.clearInterval(this.intervalId)
        this.props.onChange({name: '', url: ''})
        this.setState({uploading: false, progress: 0, error: ''})
    }

    handleUploadClick = () => {
        this.refs.fileInput.dispatchEvent(new MouseEvent('click', {
            'view': window,
            'bubbles': true,
            'cancelable': true
        }));
    }

    render() {
        // const {FilePreview: filePreviewComponent, ButtonUpload: buttonUploadComponent, ProgressBar: progressBarComponent} = this.props
        const FilePreview = this.props.filePreviewComponent
        const ButtonUpload = this.props.buttonUploadComponent
        const ProgressBar = this.props.progressBarComponent
        const {error, uploading, progress} = this.state
        const {value = {}, id} = this.props
        const {url, name} = value
        return (
            <section className="upload-container">
                <input
                    id={id}
                    type='file'
                    name={name}
                    onChange={this.uploadFile}
                    ref='fileInput'
                    disabled={uploading}
                    style={{display: 'none'}}
                />
                {url &&
                <FilePreview
                    changeClick={this.handleUploadClick}
                    cancelClick={this.handleCancelClick}
                    url={url}
                    name={name}/> }
                {!url && !uploading &&
                <ButtonUpload onClick={this.handleUploadClick}/>  }
                {uploading &&
                <ProgressBar progress={progress} onClick={this.handleCancelClick}/> }
                {error && <Label basic color='red' pointing>{error}</Label>}
            </section>
        );
    }
}


export const FilePreview = ({url, name, changeClick, cancelClick}) => (
    <div className='reveal-content'>
        <Image src={url} bordered wrapped className='visible'/>
        <Button.Group vertical>
            <Button  size="tiny"  secondary icon='edit' onClick={changeClick}>Change</Button>
            <Button  size="tiny"  primary icon='download' as="a" href={url} rel='noopener' target='_blank' title={name}>Download</Button>
            <Button  size="tiny"  negative icon='trash outline' onClick={cancelClick}> Remove</Button>
        </Button.Group>
    </div>
)


export const ButtonUpload = ({onClick}) => (
    <Button onClick={onClick}>
        Choose File ...
    </Button>
)

export const ProgressBar = ({progress, onClick}) => (
    <Progress percent={progress} progress onClick={onClick}/>
)
FieldUpload.defaultProps = {
    filePreviewComponent: FilePreview,
    buttonUploadComponent: ButtonUpload,
    progressBarComponent: ProgressBar,
}


export default  connectField(FieldUpload);

