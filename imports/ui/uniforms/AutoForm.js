import {AutoForm} from 'uniforms-react-semantic'; // or any other

/**
 * Removes recursively properties from a object
 * @param {Object} doc to be cleaned (mutated)
 * @param {Array} [properties=['__typename']] properties to be removed from the object

 const cleanProperties = function cleanProperties(doc, properties = ['__typename']) {
    if (doc instanceof Array) {
        for (let i = 0; i < doc.length; i++) {
            cleanProperties(doc[i]);
        }
    } else {
        for (const prop in doc) {
            if (properties.indexOf(prop) >= 0) {
                delete doc[prop]
            } else if (doc[prop] instanceof Object || doc[prop] instanceof Array) {
                cleanProperties(doc[prop]);
            }

        }
    }
    return doc
}
 */
const MyAutoForm = parent => class extends parent {

    static MyAutoForm = MyAutoForm;

    static MyAutoForm = `MyAutoForm${parent.displayName}`;

    getNativeFormProps() {
        //noinspection JSUnusedLocalSymbols
        const {
            autosave,        // eslint-disable-line no-unused-vars
            autosaveDelay,   // eslint-disable-line no-unused-vars
            disabled,        // eslint-disable-line no-unused-vars
            label,           // eslint-disable-line no-unused-vars
            model,           // eslint-disable-line no-unused-vars
            modelTransform,  // eslint-disable-line no-unused-vars
            onChange,        // eslint-disable-line no-unused-vars
            onSubmit,        // eslint-disable-line no-unused-vars
            onSubmitFailure, // eslint-disable-line no-unused-vars
            onSubmitSuccess, // eslint-disable-line no-unused-vars
            placeholder,     // eslint-disable-line no-unused-vars
            schema,          // eslint-disable-line no-unused-vars
            showInlineError, // eslint-disable-line no-unused-vars
            mapServerError,
            ...props
        } = this.props;

        const error = !!this.getChildContextError();

        return {
            ...props,
            error,
            onSubmit: this.onSubmit,
            key: `reset-${this.state.resetCount}`
        };
    }

    onSubmit(event) {
        if (event) {
            event.preventDefault();
            event.stopPropagation();
        }

        const promise = Promise.resolve(
            this.props.onSubmit &&
            this.props.onSubmit(this.getModel('submit'))
        );

        return promise.then(
            this.props.onSubmitSuccess,
            /** handle errors coming from server **/
            (err) => {
                console.log('err', err)
                const errors = this.props.mapServerError.call(this, err)
                if (errors.length) {
                    this.setState({error: {details: errors}})
                }
                if (typeof this.props.onSubmitFailure === 'function') this.props.onSubmitFailure(err)
            }
        );
    }

    static defaultProps = {
        mapServerError(err){
            const schema = this.getChildContextSchema();
            //todo delete next line
            window.schema = schema
            const errors = []
            const fields = schema.schema._schemaKeys
            let message = (err.reason || err.message)
            if (!Array.isArray(message)) message = [message]
            message.forEach((msg) => {
                fields.forEach((field) => {
                    const props = schema.getProps(field)
                    const label = props && props.label || field
                    if (msg.indexOf(label) >= 0) {
                        console.log('msg', msg, 'field', field, 'label', label)
                        errors.push({name: field, message: msg.replace(new RegExp(field, 'gi'), label)})
                    }
                })
            })

            if (!errors.length) { //noinspection JSCheckFunctionSignatures
                console.log('message no field',message)
                errors.push({message: message.join(' ')})
            }
            return errors
        },
        ...parent.defaultProps
    }
};


export default AutoForm.Validated(MyAutoForm(AutoForm));

/*
 onSubmitFailure(err)
 {
 console.log('onSubmitFailure', err)
 //todo maybe do a function for recognize any word in the reason || message and try to attached to the field

 }

 onSubmitSuccess()
 {
 this.setState({error: null})
 if (typeof this.props.onSubmitSuccess === 'function') return this.props.onSubmitSuccess()
 //todo notification
 //todo redirect
 }

 onChange(key, value)
 {
 let errors = this.state.error && this.state.error.details
 if (!errors) return
 let stayErrors = _.reject(errors, e => e.name === key)
 if (stayErrors.length === errors.length) return stayErrors = [] //if are the same erros is because the errors comes from sever
 this.setState({error: stayErrors.length ? stayErrors : null})
 if (typeof this.props.onChange === 'function') return this.props.onChange(key, value)
 }
 */