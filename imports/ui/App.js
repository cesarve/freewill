import React from 'react';
import '/imports/ui/styles/semantic/dist/semantic.css';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import {IntlProvider, addLocaleData} from 'react-intl';
import {withTracker} from 'meteor/react-meteor-data'
import en from 'react-intl/locale-data/en';
import HeaderPrivate from '/imports/ui/components/Header/HeaderSimple'
import HeaderPublic from '/imports/ui/components/Header/Header'
import Footer from '/imports/ui/components/Home/Footer'


let Public, User, Admin, LazyLoaderRoute
if (Meteor.isDevelopment) {
    Public = require('/imports/ui/Public').default
    User = require('/imports/ui/User').default
    Admin = require('/imports/ui/Admin').default

} else {
    LazyLoaderRoute = require('/imports/ui/components/Util/LazyLoaderRoute').default
}


addLocaleData([...en/*, ...es, ...fr, ...it*/]);


const App = () => {
    const language = (navigator.languages && navigator.languages[0]) ||
        navigator.language ||
        navigator.userLanguage;

    return (
        <IntlProvider locale={language}>
            <Router>
                <div>
                    {Meteor.userId() ? <HeaderPrivate/> : <HeaderPublic/> }

                    {Meteor.isDevelopment && <Route path="/" component={Public}/> }
                    {Meteor.isDevelopment && <Route path="/user" component={User}/>  }
                    {Meteor.isDevelopment && <Route path="/admin" component={Admin}/> }

                    {Meteor.isProduction && <LazyLoaderRoute path="/" from='/imports/ui/Public'/>}
                    {Meteor.isProduction && <LazyLoaderRoute path="/user" from='/imports/ui/User'/>}
                    {Meteor.isProduction && <LazyLoaderRoute path="/admin" from='/imports/ui/Admin'/>}

                    <Footer/>
                </div>
            </Router>
        </IntlProvider>
    )
};
/*
 <Route exact path="/sign-up/verify" from='/imports/ui/pages/Auth/SignUpVerify'/>
 <Route exact path="/sign-up/verify" from='/imports/ui/pages/Auth/Verify'/>
 <Route exact path="/sign-in" from='/imports/ui/pages/Auth/SignIn'/>

 App.propTypes = {
 currentUser: PropTypes.object,
 hasErrors: PropTypes.bool,
 refetch: PropTypes.func,
 userLoading: PropTypes.bool,
 };
 */


import './importer'


export default App
