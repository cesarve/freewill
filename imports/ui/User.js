import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {withTracker} from 'meteor/react-meteor-data'
import {isUser} from "../api/util";
import {Dimmer, Grid, Loader, Segment} from "semantic-ui-react";
import {Redirect, Route} from 'react-router-dom'



class User extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {loading, isUser} = this.props
        if (loading) return (<Dimmer active page inverted><Loader/></Dimmer>)
        if (!loading && !isUser) return (<Redirect to="/sign-in"/>)
        return (
            <Grid >
                <Grid.Row>
                    <Grid.Column width={3}>
                    </Grid.Column>
                    <Grid.Column width={13}>
                        <Segment basic>


                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}


const UserWithTracker = withTracker(() => {
    const handle = Meteor.subscribe('me')
    const loading = !handle.ready()

    const user=Meteor.user()
    return {
        loading,
        isUser: isUser(Meteor.userId()),
    }
})(User)

UserWithTracker.displayName = 'User'

export default UserWithTracker
