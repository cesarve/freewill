import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {withTracker} from 'meteor/react-meteor-data'
import {isAdmin} from "../api/util";
import {Dimmer, Grid, Loader, Segment} from "semantic-ui-react";
import {Redirect, Route} from 'react-router-dom'
import Users from '/imports/ui/components/Admin/Users/Users'
import UserEdit from "/imports/ui/components/Admin/Users/UserEdit";
import UserHistory from "/imports/ui/components/Admin/Users/UserHistory";

//todo remove this file and other layouts

class Admin extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        const {loading, isAdmin} = this.props
        if (loading) return (<Dimmer active page inverted><Loader/></Dimmer>)
        if (!loading && !isAdmin) return (<Redirect to="/sign-in"/>)
        return (
            <Grid >
                <Grid.Row>
                    <Grid.Column width={3}>
                    </Grid.Column>
                    <Grid.Column width={13}>
                        <Segment basic>
                            <Route path="/admin/users" component={Users}/>
                            <Route path="/admin/users/:userId/edit" component={UserEdit}/>
                            <Route path="/admin/users/:userId/history" component={UserHistory}/>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}


const AdminWithTracker = withTracker(() => {
    const handle = Meteor.subscribe('me')
    const loading = !handle.ready()
    return {
        loading,
        isAdmin: isAdmin(Meteor.userId()),
    }
})(Admin)

AdminWithTracker.displayName = 'Admin'

export default AdminWithTracker

