import React, {Component} from 'react';
import LazyLoaderRoute from '/imports/ui/components/Util/LazyLoaderRoute';
import '/imports/ui/styles/semantic/dist/semantic.css';
import {withTracker} from 'meteor/react-meteor-data'
import Mission from '/imports/ui/components/Home/Mission'
import How from '/imports/ui/components/Home/How'
import More from '/imports/ui/components/Home/More'
import About from '/imports/ui/components/Static/About'
import Plan from '/imports/ui/components/Static/Plan'
import Blog from '/imports/ui/components/Static/Blog'
import Sharing from '/imports/ui/components/Static/Sharing'
import Term from '/imports/ui/components/Static/Term'
import SignUp from '/imports/ui/components/Auth/SignUp'
import SignUpProfile from '/imports/ui/components/Auth/SignUpProfile'
import SignUpVerify from '/imports/ui/components/Auth/SignUpVerify'
import SignInSocialOrEmail from '/imports/ui/components/Auth/SignInSocialOrEmail'
import SignUpSocialOrEmail from '/imports/ui/components/Auth/SignUpSocialOrEmail'
import SignUpEmailVerify from  '/imports/ui/components/Auth/SignUpEmailVerify'

import {Route} from "react-router-dom";


class Public extends Component {
    constructor(props) {
        console.log('PublicPublicPublicPublicPublicPublicPublic')
        super(props)
    }

    render() {
        return (
            <div>
                <Route exact path="/" component={Mission}/>
                <Route exact path="/" component={How}/>
                <Route exact path="/" component={More}/>
                <Route exact path="/about" component={About}/>
                <Route exact path="/plan" component={Plan}/>
                <Route exact path="/blog" component={Blog}/>
                <Route exact path="/sharing" component={Sharing}/>
                <Route exact path="/term" component={Term}/>
                <Route exact path="/sign-up" component={SignUpSocialOrEmail}/>
                <Route exact path="/sign-up-email" component={SignUp}/>
                <Route exact path="/sign-up/profile" component={SignUpProfile}/>
                <Route exact path="/sign-up/verify" component={SignUpVerify}/>
                <Route exact path="/sign-in" component={SignInSocialOrEmail}/>
                <Route exact path="/verifying-email/:userId/:emailCode" component={SignUpEmailVerify}/>
            </div>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}


export default Public
