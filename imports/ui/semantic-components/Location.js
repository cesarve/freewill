/**
 * Created by cesar on 4/9/17.
 */
import PropTypes from 'prop-types'
import React, {Component}     from 'react';
import {Dropdown, Image, Label}  from 'semantic-ui-react'


class Location extends Component {
    constructor(props) {
        super(props)
        this.options = []
        this.state = {options: [], loading: false}
    }

    componentDidMount() {
        this.handleNewProps(this.props)
    }

    handleNewProps = (props) => {
        let {value = {}} = props
        this.setState({fullValue: value})
        const { description} = value
        if (description) this.autocompleteService({input: description})
    }

    componentWillReceiveProps(nextProps) {
        this.handleNewProps(nextProps)
    }

    /*
     getPosition = () => {
     if (navigator.geolocation) {
     navigator.geolocation.getCurrentPosition((position) => {
     const {autocompletionRequest = {}} = this.props
     const {coords: {latitude: lat, longitude: lng}} = position
     this.setState({location: new google.maps.LatLng({lat, lng})})
     }, (err) => {
     console.error(err)
     });
     }
     }
     */


    autocompleteService = ({input = ''}) => {
        this.setState({loading: true})
        const {autocompletionRequest = {}} = this.props
        if (input) autocompletionRequest.input = input
        const autocompleteService = new google.maps.places.AutocompleteService()
        autocompleteService.getPlacePredictions(autocompletionRequest, (autocompletePrediction, placesServiceStatus) => {
            this.setState({loading: false})
            if (placesServiceStatus === google.maps.places.PlacesServiceStatus.OK) {
                this.options = autocompletePrediction
                const options = autocompletePrediction.map((option) => {
                    return {
                        text: option.description,
                        value: option.place_id,
                    }
                })
                this.setState({options})
            } else {
                console.error(placesServiceStatus)
            }

        },)
    }
    handleSearch = (e, value) => {
        const {onSearchChange} = this.props
        if (onSearchChange) onSearchChange(value)
        if (!value) {
            this.setState({options: [], loading: false})
            this.handleChange(e, {value})
            return
        }
        this.autocompleteService({input: value})

    }
    getFullValue = (value) => {
        for (const option of this.options) {
            if (option.place_id === value) return option
        }
    }
    handleChange = (e, data) => {
        const value = this.getFullValue(data.value)
        this.setState({value})
        this.props.onChange(e, value)
    }

    render() {
        const {fullValue = {}, loading, options} = this.state
        const {autocompletionRequest, ...dropdownOptions} = this.props
        const {place_id, description} = fullValue
        return (
            <Dropdown
                {...dropdownOptions}
                options={[...options]}
                onSearchChange={this.handleSearch}
                loading={loading}
                value={place_id}
                onChange={this.handleChange}
            />
        )
    }

    static propTypes = {
        key: PropTypes.string
    }
    static defaultProps = {
        search: true,
        selection: true,
        header: <Dropdown.Header content={<Image style={{minWidth: "120px"}} src="https://developers.google.com/maps/documentation/places/images/powered-by-google-on-white.png"/>}/>

    }
}

/**
 * return if a result is in accepted types
 * https://developers.google.com/places/supported_types#table3
 * @param {Array} acceptedTypes The types of predictions to be returned. For a list of supported types, see the developer's guide. If nothing is specified, all types are returned. In general only a
 * single type is allowed. The exception is that you can safely mix the 'geocode' and 'establishment' types, but note that this will have the same effect as specifying no types.
 * @param {Array} types types in result
 */
const match = function (acceptedTypes = [], types = []) {

    if (acceptedTypes.length === 0) return true
    if (acceptedTypes.indexOf("(cities)") >= 0) {
        acceptedTypes = [...acceptedTypes, "locality", "administrative_area_level_3"]
    }

    if (acceptedTypes.indexOf("(regions)") >= 0) {
        acceptedTypes = [...acceptedTypes, "locality", "sublocality", "postal_code", "country", "administrative_area_level_1", "administrative_area_level_2",]
    }
    for (const type of types) {
        if (acceptedTypes.indexOf(type) >= 0) return true
    }
    return false
}

export default Location

