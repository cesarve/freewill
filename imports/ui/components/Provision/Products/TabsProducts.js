/*
 * Created by Jose Carmona on 11-10-2017
 */
import React, {Component} from 'react'
import { Menu, Tab, Segment } from 'semantic-ui-react'
import Skill from './Skill'
import Products from './Products'
import TabsProducts2 from './TabsProducts2'
import Creator from "../../Creators/Creator";

class TabProducts extends Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        Qty_abilities: 11,
        img1: '/images/imagen1.png',
        city: 'Cebu City, Philippines',
        img2: 'img/default-avatar.png',
        name: 'Jonathan Eraser',
        creator: 'kikfreewilltesting1',
        creator_message: 'The best is yet to come',
        level: 'Advance',
        description: 'I am an excel expertand I can create style code and macros.',
        val1: 1,
        val2: 1,
        val3: 1,
        ability: 'Excel work',
        btn1: 'ADD AN ABILITY',
        btn2: 'SEARCH FOR AVAILABLE ABILITIES',
    }       

    render(){
        const {Qty_abilities, img1, city, ability, img2, name, level, description, val1, val2, val3, creator, creator_message } = this.props    
	    const panes = [
	          { menuItem: 'Hilighted Products', render: () => <Tab.Pane attached={false}><Products /></Tab.Pane> },
	          { menuItem: 'Offered Products', render: () => <Tab.Pane attached={false}><TabsProducts2 /></Tab.Pane> },
	          { menuItem: 'Requested Products', render: () => <Tab.Pane attached={false}><TabsProducts2 /></Tab.Pane> },
	        ]

    	return(
            <Segment stacked>
        		<Creator creator={creator} message={creator_message} />
        		<Tab  menu={{  attached: true, tabular: true, color: 'green', stackable: true }} panes={panes} />
        		<Skill city={city} ability={ability} img={img2} img2={img1} name={name} level={level}
                                               description={description} val1={val1} val2={val2} val3={val3}/>
    	    </Segment>
        )

    }

 }   

 export default TabProducts
        