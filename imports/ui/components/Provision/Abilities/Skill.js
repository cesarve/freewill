/**
 * Created by Jose Carmona on 08/10/17.
 */
import React , { Component } from 'react'
import { Image, Grid, Button, Div, Segment, Icon } from 'semantic-ui-react'
import Provide from './../Provide/Provide'

//ract intl
import { addLocaleData , IntlProvider, FormattedMessage} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import localeData from './../data.json';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = localeData[language] || localeData.en;

class Skill extends Component{
	constructor (props) {
		super(props);
		console.log("-----descripcion que viene del state de TabAbilities: ",this.props.state.description);
	}

	render(){
		return(
			<Segment clearing color='green'>
				<Grid columns='equal'>
					<Grid.Row >
						<Grid.Column>
							<Image src={this.props.img} />
						</Grid.Column>

						<Grid.Column>
							<h5>
								<FormattedMessage
								  id={ 'Provision.Skill.ability_city' }
								  defaultMessage={ '{ability} (Available: {city})' }
								  values={{ city: this.props.city, ability: this.props.ability }}
								/>
	                    	</h5>
	                    	
							<Provide img={this.props.img} name={this.props.name} level={this.props.level}/>

							<Grid>
								<label>
									<FormattedMessage
									  id={ 'Provision.Skill.description' }
									  defaultMessage={ '{description}' }
									  values={{ description: this.props.state.description }}
									/>	
								</label>
							</Grid>	
						</Grid.Column>

						<Grid.Column textAlign="right">
							<h5>
								<Button label={this.props.val1} color='orange' icon='marker' labelPosition='left' />
							</h5>
							<h5>
								<Button label={this.props.val2} color='orange' icon='like outline' labelPosition='left' />
							</h5>
							<h5>
								<Button label={this.props.val3} color='orange' icon='dislike outline' labelPosition='left' />
							</h5>

							<h5>
								<FormattedMessage
								  id={ 'Provision.Skill.status' }
								  defaultMessage={ '{status}' }
								  values={{ status: this.props.state.status }}
								/>	
							</h5>
						</Grid.Column>
					</Grid.Row>
				</Grid>

				<Grid columns='equal'>
					<Grid.Row >
						<Grid.Column>
							<label>
								<FormattedMessage
								  id={ 'Provision.Skill.action' }
								  defaultMessage={ '{action}' }
								  values={{ action: this.props.state.labelAction }}
								/>	
							</label>
						</Grid.Column>

						<Grid.Column>
							<label>
								{this.props.state.time}
							</label>
						</Grid.Column>

						<Grid.Column>
							<Button floated='right' color='green'> {this.props.state.btn} </Button>
						</Grid.Column>
					</Grid.Row>
				</Grid>	
			</Segment>
			)
	}
}

export default Skill