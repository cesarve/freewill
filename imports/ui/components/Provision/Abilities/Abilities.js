/*
 * Created by Jose Carmona on 08-10-2017
 */
import React, {Component} from 'react'
import {schema} from './../../../../api/Provision/Abilities/abilitiesSchema'
import AutoForm from  "../../../uniforms/AutoForm"
import AutoField from 'uniforms-react-semantic/AutoField'
import TextField from 'uniforms-react-semantic/TextField'
import SubmitField from 'uniforms-react-semantic/SubmitField'
import ErrorsField from 'uniforms-react-semantic/ErrorsField'
import Skill from './Skill'
import SelectField from 'uniforms-react-semantic/SelectField'
import {Menu, Button, Divider, Segment, Header, Image, Div, Grid, Form, TextArea, Select, options, Accordion, Icon, Tab} from 'semantic-ui-react'
import {FormattedMessage} from 'react-intl';
import Creator from "../../Creators/Creator";

class Abilities extends Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        Qty_abilities: 11,
        img1: '/images/imagen1.png',
        city: 'Cebu City, Philippines',
        img2: 'img/default-avatar.png',
        name: 'Jonathan Eraser',
        creator: 'kikfreewilltesting1',
        creator_message: 'The best is yet to come',
        level: 'Advance',
        description: 'I am an excel expertand I can create style code and macros.',
        val1: 1,
        val2: 1,
        val3: 1,
        ability: 'Excel work',
    }

    render() {
        const {Qty_abilities, img1, city, ability, img2, name, level, description, val1, val2, val3, creator, creator_message } = this.props	
        return (
            <div>
                    <Header as='h2' textAlign='center' disabled>
                        <FormattedMessage
                            id={ 'Provision.Abilities.header' }
                            defaultMessage={ 'ABILITIES' }
                        />
                    </Header>

                    <AutoForm schema={schema} showInlineError>

                        <Grid columns='equal' padded stackable>
                            <Grid.Row>
                                <Grid.Column>
                                    <Form.Field >
                                        <AutoField icon='search' placeholder='Any abilities' name='present'/>
                                    </Form.Field >
                                </Grid.Column>
                                <Grid.Column>
                                    <Form.Field >
                                        <AutoField icon='search' placeholder='On Gold Coast' name='availableIn'/>
                                    </Form.Field >
                                </Grid.Column>
                                <Grid.Column>
                                    <Form.Field >
                                        <SelectField name='competency' fluid selection placeholder='Any levels'/>
                                    </Form.Field >
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>

                        <Accordion >
                            <Accordion.Title>
                                <h4>
                                    <FormattedMessage
                                        id={ 'Provision.Abilities.avanced_search' }
                                        defaultMessage={ 'Avanced Search' }
                                    />
                                    <Icon name='dropdown' size={"large"}/>
                                </h4>
                            </Accordion.Title>
                                 
                            <Accordion.Content >
                                <Grid columns='equal' padded stackable>
                                    <Grid.Row>
                                        <Grid.Column >
                                            <Form.Field >
                                                <SelectField name='shareBy' fluid selection placeholder='Any creators'/>
                                            </Form.Field >
                                        </Grid.Column>
                                        <Grid.Column >
                                            <Form.Field >
                                                <SelectField name='gender' fluid selection placeholder='Any'/>
                                            </Form.Field >
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Grid columns='equal'>
                                                <Grid.Row>
                                                    <Grid.Column >
                                                        <Form.Field >
                                                            <AutoField placeholder='Any' name='age.to'/>
                                                        </Form.Field >
                                                    </Grid.Column>
                                                    <Grid.Column >
                                                        <Form.Field>
                                                            <AutoField placeholder='Any' name='age.from'/>
                                                        </Form.Field >
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column>
                                            <Form.Field >
                                                <SelectField name='provided' fluid selection placeholder='Freely or for power'/>
                                            </Form.Field >
                                        </Grid.Column>

                                        <Grid.Column>
                                            <Form.Field >
                                                <AutoField placeholder='Any date' name='provided.on'/>
                                            </Form.Field >
                                        </Grid.Column>
                                        <Grid.Column>
                                            <Form.Field >
                                                <AutoField placeholder='Any time' name='provided.at'/>
                                            </Form.Field >
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                                <Divider/>
                            </Accordion.Content>
                        </Accordion>
                    
                        <Grid columns='equal' width={12}>
                            <Grid.Row>
                                <Grid.Column>
                                    <FormattedMessage
                                        id={ 'Provision.Abilities.qty_abilities' }
                                        defaultMessage={ '{qty_abilities} Abilities' }
                                        values={{qty_abilities: Qty_abilities}}
                                    />
                                </Grid.Column>
                                
                                <Grid.Column>
                                    <label> {this.props.btn} </label>
                                </Grid.Column>

                                <Grid.Column textAlign="right">
    								<AutoField placeholder='Hilights' name='arrangeBy'/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
				    </AutoForm>
            </div>
        )
    }
}


export default Abilities