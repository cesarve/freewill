import React, {Component} from 'react'
import { Image, Menu, Tab, Segment, Grid } from 'semantic-ui-react'
//ract intl
import { addLocaleData , IntlProvider, FormattedMessage} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import localeData from './../data.json';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = localeData[language] || localeData.en;

class Provide extends Component {
    constructor(props) {
        super(props);
    }

  render(){
     
    	return(
           <Grid columns='equal'>
            	<Grid.Row >
                	<Grid.Column width={4}>
						<Image src={this.props.img} />
					</Grid.Column>

					<Grid.Column width={12}>
						<label>
							<FormattedMessage
							  id={ 'Provision.Skill.name_level' }
							  defaultMessage={ 'Provided by {name} ({level})' }
							  values={{ name: this.props.name, level: this.props.level }}
							/>	
						</label>
					</Grid.Column>
				</Grid.Row>
			</Grid>
        )

    }

 }   

 export default Provide
        
