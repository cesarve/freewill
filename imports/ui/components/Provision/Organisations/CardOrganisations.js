/**
 * Created by Jose Carmona on 08/10/17.
 */
import React , { Component } from 'react'
import { Image, Grid, Button, Div, Segment, Icon } from 'semantic-ui-react'

//ract intl
import { addLocaleData , IntlProvider, FormattedMessage} from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';
import localeData from './../data.json';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = localeData[language] || localeData.en;

class CardOrganisations extends Component{
	constructor (props) {
		super(props);
	}

	render(){
		return(
			<Segment clearing color='green'>
				<Grid columns='equal'>
					<Grid.Row >
						<Grid.Column>
							<Image src={this.props.img} />
						</Grid.Column>

						<Grid.Column>
							<h5>{this.props.name} ({this.props.type})</h5>
							<h5>{this.props.web}</h5>
							<FormattedMessage
								  id={ 'Provision.CardOrganisations.description' }
								  defaultMessage={ '{description}' }
								  values={{ description: this.props.description}}
							/>	
						</Grid.Column>

						<Grid.Column textAlign="right">
							<h5>
								<Button label={this.props.val1} color='orange' icon='marker' labelPosition='left' /> 
							</h5>
							<h5>
								<Button label={this.props.val2} color='orange' icon='like outline' labelPosition='left' />
							</h5>
							<h5>
								<Button label={this.props.val3} color='orange' icon='dislike outline' labelPosition='left' />
							</h5>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</Segment>	

			)
	}
}

export default CardOrganisations