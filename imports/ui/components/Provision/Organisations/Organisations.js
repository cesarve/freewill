/*
 * Created by Jose Carmona on 11-10-2017
 */
import React, {Component} from 'react'
import AutoForm from  "../../../uniforms/AutoForm"
import AutoField from 'uniforms-react-semantic/AutoField'
import TextField from 'uniforms-react-semantic/TextField'
import SubmitField from 'uniforms-react-semantic/SubmitField'
import ErrorsField from 'uniforms-react-semantic/ErrorsField'
import {schema} from './../../../../api/Provision/Organisations/organisationsSchema'
import CardOrganisations from './CardOrganisations'
import SelectField from 'uniforms-react-semantic/SelectField'
import {Menu, Button, Divider, Segment, Header, Image, Div, Grid, Form, TextArea, Select, options, Accordion, Icon, Tab} from 'semantic-ui-react'
import {FormattedMessage} from 'react-intl';
import Creator from "../../Creators/Creator";

class Organisations extends Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        Qty_Organisations: 1,
        img: 'img/default-avatar.png',
        name: 'TAMARIVAS',
        type: 'live',
        creator: 'kikfreewilltesting1',
        creator_message: 'The best is yet to come',
        description: 'This is great',
        web: 'www.test.com',
        val1: 1,
        val2: 1,
        val3: 1,
    }

    state = {activeItem: 'Hilighted'}

    handleItemClick = (e, {name}) => this.setState({activeItem: name})

    render() {
        const {activeItem} = this.state
        const {Qty_Organisations, img, name, type, web, description, val1, val2, val3, creator, creator_message, date } = this.props	
        return (
            <div>
                <Segment stacked>
                    <Creator creator={creator} message={creator_message} />
                    <Header as='h2' textAlign='center' disabled>
                        <FormattedMessage
                            id={ 'Provision.Organisations.header' }
                            defaultMessage={ 'ORGANISATIONS' }
                        />
                    </Header>

                    <AutoForm schema={schema} showInlineError>
                        <Grid columns='equal' padded stackable>
                            <Grid.Row>
                                <Grid.Column>
                                    <Form.Field >
                                        <AutoField icon='search' placeholder='Any Products' name='present'/>
                                    </Form.Field >
                                </Grid.Column>
                                <Grid.Column>
                                    <Form.Field >
                                        <AutoField icon='search' placeholder='Any Products' name='relevantTo'/>
                                    </Form.Field >
                                </Grid.Column>
                                <Grid.Column>
                                    <Form.Field >
                                        <AutoField icon='search' placeholder='New or used' name='type'/>
                                    </Form.Field >
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>

                        <Grid container>
                            <Grid.Column width={3}>
                                <FormattedMessage
                                    id={ 'Provision.Organisations.qty_Organisations' }
                                    defaultMessage={ '{qty_Organisations} Organisations' }
                                    values={{qty_Organisations: Qty_Organisations}}
                                />
                            </Grid.Column>
                            <Grid.Column textAlign="center" width={10}>
                                <Icon color="orange" name='add circle icon' size={"large"}/>
                                <FormattedMessage
                                    id={ 'Provision.Organisations.atract_others_users' }
                                    defaultMessage={ 'ADD AN ORGANISATION' }
                                />
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <Grid.Column textAlign="right">
                                    <AutoField placeholder='Hilights' name='arrangeBy'/>
                                </Grid.Column>
                            </Grid.Column>
                        </Grid>

                    </AutoForm>
                        
                    <CardOrganisations img={img} name={name} type={type} description={description} web={web} val1={val1} val2={val2} val3={val3}/>

                </Segment>
            </div>
        )
    }
}

export default Organisations