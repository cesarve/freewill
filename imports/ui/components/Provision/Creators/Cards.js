import { Segment, Grid, Div, Button, Icon, Image } from 'semantic-ui-react'
import {FormattedMessage} from 'react-intl'
import React, {Component} from 'react'

class Cards extends Component {
    constructor(props) {
        super(props);
    }

   render() {
        return (            
                <Segment clearing color='green'>
                      <Grid columns='equal'>
                            <Grid.Row>
                                <Grid.Column>
                                    <Image src={this.props.img}/> 
                                </Grid.Column>                                
                                <Grid.Column>
                                    <h5>{this.props.name} </h5>
                                    <label>{this.props.description} </label>
                                </Grid.Column>
                                <Grid.Column textAlign="right"> 
                                    <h5><Button label={this.props.val1} color='orange' icon='marker' labelPosition='left' /> </h5>
                                    <h5><Button label={this.props.val2} color='orange' icon='like outline' labelPosition='left' /></h5>
                                    <Grid.Row>
                                        <Grid.Column padded>
                                            <Button color='orange'>Message</Button>
                                        </Grid.Column>            
                                        <Grid.Column padded>
                                            <Button color='orange'>Organise</Button>
                                        </Grid.Column>
                                    </Grid.Row>    
                                    <Grid padded stackable>{this.props.date}</Grid>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                </Segment>
                )
            }
}

export default Cards