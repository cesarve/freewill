/*
 * Created by Jose Carmona on 10-10-2017
 */
import React, {Component} from 'react'
import AutoForm from  "../../../uniforms/AutoForm"
import AutoField from 'uniforms-react-semantic/AutoField'
import TextField from 'uniforms-react-semantic/TextField'
import SubmitField from 'uniforms-react-semantic/SubmitField'
import ErrorsField from 'uniforms-react-semantic/ErrorsField'
import Cards from './Cards'
import SelectField from 'uniforms-react-semantic/SelectField'
import {Menu, Button, Divider, Segment, Header, Image, Div, Grid, Form, TextArea, Select, options, Accordion, Icon, Tab} from 'semantic-ui-react'
import {FormattedMessage} from 'react-intl';
import Creator from "../../Creators/Creator";

class Creators extends Component {
    constructor(props) {
        super(props);
    }

    static defaultProps = {
        Qty_Creators: 1,
        img: 'img/default-avatar.png',
        name: 'Jonathan Eraser',
        creator: 'kikfreewilltesting1',
        creator_message: 'The best is yet to come',
        description: 'This is great',
        val1: 1,
        val2: 1,
        date: Date(),
    }

    render() {
        const {Qty_Creators, img, name, description, val1, val2, creator, creator_message, date } = this.props	
        return (
            <div>
                <Header as='h2' textAlign='center' disabled>
                    <FormattedMessage
                        id={ 'Provision.Creators.header' }
                        defaultMessage={ 'CREATORS' }
                    />
                </Header>

                <Grid container>
                    <Grid.Column width={3}>
                        <FormattedMessage
                            id={ 'Provision.Creators.qty_Creators' }
                            defaultMessage={ '{qty_Creators} Creators' }
                            values={{qty_Creators: Qty_Creators}}
                        />
                    </Grid.Column>
                    <Grid.Column textAlign="center" width={10}>
                        <Icon color="orange" name='add circle icon' size={"large"}/>
                        <FormattedMessage
                            id={ 'Provision.Creators.atract_others_users' }
                            defaultMessage={ 'ATTRACT OTHERS TO USE FREEWILL' }
                        />
                    </Grid.Column>
                    <Grid.Column width={3}></Grid.Column>
                </Grid>
            </div>
        )
    }
}

export default Creators