/*
 * Created by Jose Carmona on 11-10-2017
 */
import React, {Component} from 'react'
import { Menu, Tab, Segment } from 'semantic-ui-react'
import Cards from './Cards'
import Creators from './Creators'
import Creator from "../../Creators/Creator";

class TabCreators extends Component {
    constructor(props) {
        super(props);
    }

     static defaultProps = {
        Qty_Creators: 1,
        img: 'img/default-avatar.png',
        name: 'Jonathan Eraser',
        creator: 'kikfreewilltesting1',
        creator_message: 'The best is yet to come',
        description: 'This is great',
        val1: 1,
        val2: 1,
        date: Date(),
    }      

    render(){
        const {Qty_Creators, img, name, description, val1, val2, creator, creator_message, date } = this.props  
	    const panes = [
	          { menuItem: 'Hilighted', render: () => <Tab.Pane attached={false}><Creators /></Tab.Pane> },
	          { menuItem: 'Hilighted By', render: () => <Tab.Pane attached={false}><Creators /></Tab.Pane> },
	          { menuItem: 'Attracted', render: () => <Tab.Pane attached={false}><Creators /></Tab.Pane> },
	        ]

    	return(
            <Segment stacked>
        		<Creator creator={creator} message={creator_message} />
        		<Tab menu={{  attached: true, tabular: true, color: 'green', stackable: true }} panes={panes} />
        		<Cards img={img} name={name} description={description} val1={val1} val2={val2} date={date}/>
    	    </Segment>
        )

    }

 }   

 export default TabCreators
        