import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button} from "semantic-ui-react";
import InsertUniforms from '../../uniforms/InsertUniforms'
import BaseForm from 'uniforms/BaseForm'

/**
 * Button to complete a async function when click
 * Loading state while is resolving the promise onCLick
 * receive as context findError,findField,findValue,id,fields
 */
class Action extends InsertUniforms {


    constructor(props, context) {
        super(props, context)
        this.state = {loading: false, disabled: false, icon: props.initialIcon}
    }


    handleClick = () => {
        const {initialIcon: icon, readyIcon} = this.props
        this.setState({loading: true, disabled: true})
        this.props.onClick.call(this.getFunctionFromContext())
            .then(() => {
                this.setState({loading: false, icon: readyIcon})
                setTimeout(() => this.setState({icon, disabled: false}), 10000)
            })
            .catch(() => {
                this.setState({loading: false, icon, disabled: false})
            })
    }

    render() {

        const {content} = this.props
        const {loading, disabled, icon} = this.state
        return (
            <Button type="button" loading={loading} disabled={disabled} icon={icon} content={content} onClick={this.handleClick}/>
        )
    }

    static defaultProps = {
        initialIcon: 'refresh',
        readyIcon: 'check',
    }

    static propTypes = {
        /** {function} onClick - Action on click, have to return a promise **/
        onClick: PropTypes.func.isRequired,
        /** {string|Component} content of the button **/
        content: PropTypes.string.isRequired,
        /** {string} initialIcon show as initial Icon **/
        initialIcon: PropTypes.string,
        /** {string} readyIcon showed when the promise is resolved  **/
        readyIcon: PropTypes.string,
    }
}


export default Action
