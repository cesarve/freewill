import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button} from "semantic-ui-react";
import InsertUniforms from '../../uniforms/InsertUniforms'
import BaseForm from 'uniforms/BaseForm'

/**
 * Button to complete a async function when click
 * Loading state while is resolving the promise onCLick
 * receive as context findError,findField,findValue,id,fields
 */
class Action extends Component {


    constructor(props, context) {
        super(props, context)
        this.state = {loading: false, disabled: false, icon: props.initialIcon}
    }


    handleClick = () => {
        const {initialIcon: icon, readyIcon, value, delay} = this.props
        this.setState({loading: true, disabled: true})
        this.props.onClick(value)
            .then((res) => {
                this.setState({loading: false, icon: readyIcon})
                setTimeout(() => this.setState({icon, disabled: false}), delay)
            })
            .catch(() => {
                this.setState({loading: false, icon, disabled: false})
            })
    }

    render() {

        //noinspection JSUnusedLocalSymbols
        const {content, initialIcon, readyIcon, value, delay, ...props} = this.props
        const {loading, disabled, icon} = this.state
        return (
            <Button type="button" loading={loading} disabled={disabled} icon={icon} content={content} onClick={this.handleClick} {...props}/>
        )
    }

    static defaultProps = {
        initialIcon: 'refresh',
        readyIcon: 'check',
        delay: 2000,
    }

    static propTypes = {
        /** {function} onClick - Action on click, have to return a promise **/
        onClick: PropTypes.func.isRequired,
        /** {string|Component} content of the button **/
        content: PropTypes.string.isRequired,
        /** {string} initialIcon show as initial Icon **/
        initialIcon: PropTypes.string,
        /** {string} readyIcon showed when the promise is resolved  **/
        readyIcon: PropTypes.string,
        /** {number} milliseconds for change the status after ready  **/
        delay: PropTypes.string,
    }
}


export default Action
