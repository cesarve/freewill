import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {withTracker} from 'meteor/react-meteor-data'
import {insertReason} from "../../../api/Reasons/reasons-methods";
import AutoForm from "../../uniforms/AutoForm";
import {withRouter} from "react-router-dom";
import {Button, Dimmer, Loader, Modal} from "semantic-ui-react";

class ModalRoute extends Component {
    constructor(props) {
        super(props)
    }

    getLastLocation = () => {
        const {location} = this.props
        let lastLocation = location.pathname.replace(/\/$/, '')
        lastLocation = lastLocation.substr(0, lastLocation.lastIndexOf('/'))
        return lastLocation
    }
    onCancel = () => {
        const {history} = this.props
        history.push(this.getLastLocation())
    }


    render() {
        const {header, action, children, loading, onAction} = this.props
        return (
            <Dimmer.Dimmable as={Modal} dimmed={loading} closeOnEscape={true} closeOnRootNodeClick={true} size="small" open onClose={this.onCancel}>
                <Dimmer active={loading} inverted>
                    <Loader/>
                </Dimmer>
                <Modal.Header>
                    {header}
                </Modal.Header>
                <Modal.Content >
                    {children}
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={this.onCancel}>
                        Cancel
                    </Button>
                    <Button primary onClick={onAction}>
                        {action}
                    </Button>
                </Modal.Actions>
            </Dimmer.Dimmable>
        )
    }

    static defaultProps = {
        action: 'Submit'
    }

    static propTypes = {}
}


export default withRouter(ModalRoute)
