import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {withTracker} from 'meteor/react-meteor-data'
import AutoForm from "../../uniforms/AutoForm";
import ModalRoute from './ModalRoute'


class ModalAutoFormRoute extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {header, action, children, loading, ...props} = this.props
        let formRef
        return (
            <ModalRoute header={header} loading={loading} action={action} onAction={() => formRef.submit()}>
                <AutoForm ref={(form) => formRef = form} {...props}
                >
                    {children}
                </AutoForm>
            </ModalRoute>
        )
    }

    static defaultProps = {
        action: 'Submit'
    }

    static propTypes = {}
}


export default ModalAutoFormRoute
