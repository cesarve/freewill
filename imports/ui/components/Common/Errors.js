import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Message} from 'semantic-ui-react'


class Errors extends Component {
    constructor(props) {
        super(props)
        let {errors} = props
        console.log('props errors', errors)
    }

    render() {
        let {errors} = this.props
        console.log('errors', errors)
        if (!errors || !errors.length) return null
        if (typeof errors === 'string') errors = [errors]
        return (
            <Message negative className="text">
                <ul>{errors.map((error, i) => <li key={i}>{error}</li>)}</ul>
            </Message>
        )
    }

    static defaultProps = {}

    static propTypes = {
        errors: PropTypes.oneOf([
            PropTypes.string,
            PropTypes.array
        ])
    }
}


export default Errors

