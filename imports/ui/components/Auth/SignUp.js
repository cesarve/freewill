/**
 * Created by cesar on 22/5/17.
 */
import React from 'react';
import {Component} from 'react'
import PropTypes from 'prop-types'
import {Segment, Form, Grid, Header} from 'semantic-ui-react'
import SignUpTerms from './SignUpTerms'
import AutoField   from 'uniforms-react-semantic/AutoField'
import TextField   from 'uniforms-react-semantic/TextField'
import AutoForm from  "../../uniforms/AutoForm";
import SubmitField from 'uniforms-react-semantic/SubmitField'
import ErrorsField from 'uniforms-react-semantic/ErrorsField'
import {withTracker} from 'meteor/react-meteor-data'
import {userSignUpSchema} from '/imports/api/Users/Users'
import {toPromise} from "../../../api/util";

/**
 * Render Sign up Form
 */
class SingUp extends Component {
    /**
     * @param props
     */

    static propTypes = {
        /** columns left in mobiles (based in 16 columns)  * */
        mobileLeft: PropTypes.number,
        /**
         * columns left in tablets (based in 16 columns)
         * */
        tabletLeft: PropTypes.number,
        /**
         * columns left in computers (based in 16 columns)
         * */
        computerLeft: PropTypes.number,
        /**
         * [={}] default data, pre append data
         */
        model: PropTypes.object,
        /**
         * @callback onValidate
         * @param {object} model data inserted in form
         * @param {object} error error in the forms
         * @param {function} callback for add custom error, pass error object above
         */
        /**
         * Handle onValidate.
         * @callback onValidate
         */
        onValidate: PropTypes.func,
        /**
         * Handle submit.
         * @callback handleSubmit
         * @param {object} model data inserted in form
         */
        /**
         * {onSubmit} onSubmit handle submit callback, receives 1 params -> model,
         */
        onSubmit: PropTypes.func,
        /**
         * Handle success.
         * @callback handleSuccess
         */

        /**
         * {onSubmitSuccess} onSubmitSuccess handle validate, receive 3 params -> model, errors, callback
         */
        onSubmitSuccess: PropTypes.func,
        /**
         * Handle failure.
         * @callback handleFailure
         */

        /**
         * {onSubmitFailure} onSubmitFailure handle failure, no receive params
         */
        onSubmitFailure: PropTypes.func,

    }
    static defaultProps = {
        mobileLeft: 16,
        tabletLeft: 4,
        computerLeft: 3,
        model: {},
    }

    /**
     * @returns {XML}
     */
    render() {
        const {schema, mobileLeft, tabletLeft, computerLeft, onValidate, onSubmit, onSubmitFailure, onSubmitSuccess, model, ...props} = this.props
        const mobileRight = 16, tabletRight = 16 - tabletLeft, computerRight = 16 - computerLeft
        return (
            <div>
                <Segment basic>
                    <Header size='huge'>Join</Header>
                    <AutoForm schema={schema}
                              onValidate={onValidate}
                              onSubmit={onSubmit}
                              onSubmitSuccess={onSubmitSuccess}
                              onSubmitFailure={onSubmitFailure}
                              model={model}
                              showInlineError
                    >
                        <Grid >
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>Full name:</label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Grid stackable>
                                        <Grid.Row columns={2}>
                                            <Grid.Column>
                                                <Form.Field >
                                                    <TextField
                                                        label={false}
                                                        icon='id card outline'
                                                        iconPosition='left'
                                                        placeholder='First name'
                                                        name='firstName'/>
                                                </Form.Field>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <Form.Field >
                                                    <TextField
                                                        label={false}
                                                        icon='id card'
                                                        iconPosition='left'
                                                        placeholder='Family name'
                                                        name='familyName'/>
                                                </Form.Field>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>Email address:</label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <TextField
                                            label={false}
                                            icon='mail outline'
                                            iconPosition='left'
                                            placeholder='Email'
                                            name='email'/>
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>Create a password:</label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <TextField
                                            label={false}
                                            icon='key'
                                            iconPosition='left'
                                            type='password'
                                            placeholder='Password'
                                            name='password'/>
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>Terms & conditions:</label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}
                                             floated="right">
                                    <SignUpTerms/>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}
                                             floated="right">
                                    <AutoField
                                        name="terms"
                                        label="I agree to adhere with Freewill's terms when using Freewill.vision"/>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column  >
                                    <ErrorsField  />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column >
                                    <Form.Field >
                                        <SubmitField className="fluid" value='Join' />
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </AutoForm>
                </Segment>
            </div >
        )
    }
}


const SingUpWithTracker = withTracker(({history}) => {
    return {
        onSubmitSuccess () {
            history.replace('/sign-up/profile')
        },
        onSubmit (model) {
            console.log('Accounts.createUser BIEN', model)
            return toPromise(Accounts.createUser, {
                email: model.email,
                password: model.password,
                profile: {
                    firstName: model.firstName,
                    familyName: model.familyName,
                }
            })
        },
        schema: userSignUpSchema
    }
})(SingUp);
SingUpWithTracker.displayName = 'SingUp'

export default SingUpWithTracker