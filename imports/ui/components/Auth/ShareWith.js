import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Dropdown} from "semantic-ui-react";
import {connectField} from "uniforms";
import filterSemanticProps from 'uniforms-react-semantic/filterSemanticProps'
class ShareWith extends Component {
    constructor(props) {
        super(props)
        const {value, defaultValue} = props
        this.state = {value: value || 'everybody', windowType: ShareWith.getWindowType()}
        this.state.icon = this.getIcon(value || defaultValue)
    }

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.value !== this.props.value) {
            this.setState({value: nextProps.value})
        }
    }

    componentWillMount() {
        window.addEventListener("resize", this.onResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize);
    }

    onResize = () => {
        this.setState({windowType: ShareWith.getWindowType()})
    }

    static getWindowType() {
        const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)
        switch (true) {
            case (width <= 723):
                return 'small'
            case (width <= 933):
                return 'medium'
            default:
                return 'large'
        }
    }

    /**
     * @param index
     * @returns {*}
     *
     * Get the option index and return The Icon - Text for the options
     */
    _getOption = (index) => {
        if (index < 0) return {}
        const option = this.options[this.state.windowType][index]
        return option ? option : {}
    }

    /**
     * get the an option value and return index in options array
     * @param value
     * @returns {number}
     */
    _getIndex(value) {
        const options = this.options[this.state.windowType]
        const len = options.length
        for (let index = 0; index < len; index++) {
            if (options[index].value === value) return index
        }
        return -1
    }

    getIcon = (value) => {
        const index = this._getIndex(value)
        const option = this._getOption(index)
        return option.icon
    }
    changeIcon = (value) => {
        this.setState({icon: this.getIcon(value)})
    }
    handleChange = (e, data) => {
        this.setState({value: data.value})
        this.changeIcon(data.value)
        this.props.onChange(data.value)

    }
    options = {
        large: [
            {key: 'everybody', icon: 'users', text: 'Share with everybody', value: 'everybody'},
            {key: 'creators', icon: 'user', text: 'Share with other creators', value: 'creators'},
            {key: 'nobody', icon: 'ban', text: 'Don\'t share with others', value: 'nobody'},
        ],
        medium: [
            {key: 'everybody', icon: 'users', text: 'Everybody', value: 'everybody'},
            {key: 'creators', icon: 'user', text: 'Creators', value: 'creators'},
            {key: 'nobody', icon: 'ban', text: 'Nobody', value: 'nobody'},
        ],
        small: [
            {key: 'everybody', icon: 'users', text: '', value: 'everybody'},
            {key: 'creators', icon: 'user', text: '', value: 'creators'},
            {key: 'nobody', icon: 'ban', text: '', value: 'nobody'},
        ],
    }

    render() {
        let icon = 'dropdown'
        const {windowType, value} = this.state
        if (windowType === 'small') icon = this.state.icon
        return (
            <Dropdown options={this.options[windowType]}
                      {...filterSemanticProps(this.props)}
                      icon={icon}
                      onChange={this.handleChange}
                      value={value}
            />
        )
    }

    static defaultProps = {
        basic: true,
        button: true,
        floating: true,
    }
}

export default connectField(ShareWith);


