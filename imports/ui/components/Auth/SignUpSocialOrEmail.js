/**
 * Created by cesar on 27/9/17.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button, Divider, Header, Icon, Segment} from "semantic-ui-react";
import {withTracker} from 'meteor/react-meteor-data'
import {toPromise} from "../../../api/util";
import SocialConnect from "./SocialConnect";
import {Link} from "react-router-dom";


export default class SignUpSocialOrEmail extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Segment basic>
                <Header as="h1">Join</Header>
                <Segment padded textAlign='center'>
                    <SocialConnect redirectTo="/sign-up/profile" />
                    <Divider horizontal>Or</Divider>
                    <Button as={Link} basic color='blue' size="massive" fluid to="/sign-up-email">
                        <Icon name='mail outline'/> Your Email
                    </Button>
                </Segment>
            </Segment>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}







