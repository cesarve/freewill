import React, {Component} from 'react';
import PropTypes from 'prop-types'
import ShareWith from "./ShareWith";
import {SelectField, LongTextField, TextField, DateField, ErrorsField, SubmitField} from "uniforms-react-semantic";
import AutoForm from  "../../uniforms/AutoForm";
import {Form, Grid, Header, Icon, Segment} from "semantic-ui-react";
import CountryCodes from '/imports/ui/components/Auth/CountryCodes'
import Location from "../../uniforms/LocationField";
import {FormattedMessage} from 'react-intl'
import './signupprofile.less'
import AvatarField from "../../uniforms/AvatarField";
import {withTracker} from 'meteor/react-meteor-data'
import {userUpdateProfile} from "../../../api/Users/users-methods";

export class SignUpProfile extends Component {
    constructor(props) {
        super(props)
    }

    static propTypes = {
        mobileLeft: PropTypes.number,
        tabletLeft: PropTypes.number,
        computerLeft: PropTypes.number,
        model: PropTypes.object.isRequired
    }
    static defaultProps = {
        mobileLeft: 16,
        tabletLeft: 4,
        computerLeft: 3,

    }

    render() {
        const {schema, loading, mobileLeft, tabletLeft, computerLeft, onValidate, onSubmit, onSubmitSuccess, onSubmitFailure, model = {}, ...props} = this.props
        const mobileRight = 16, tabletRight = 16 - tabletLeft, computerRight = 16 - computerLeft
        return (
            <div className="SignUpProfile">
                <Segment basic>
                    <Header size='huge'>
                        <FormattedMessage id={ 'Auth.SignUpProfile.header' } defaultMessage={ 'Profile' }/>
                    </Header>
                    <AutoForm schema={schema}
                              loading={loading}
                              onValidate={onValidate}
                              onSubmit={onSubmit}
                              onSubmitSuccess={onSubmitSuccess}
                              onSubmitFailure={onSubmitFailure}
                              onChange={(field, data) => console.log('onChange', field, data)}
                              model={model}
                              showInlineError
                    >
                        <Grid >
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpProfile.avatar' } defaultMessage={ 'Avatar: ' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <AvatarField
                                            name="avatar.url"
                                            //slingshot={new Slingshot.Upload('avatar')}
                                        />
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpProfile.firstName' } defaultMessage={ 'First name: ' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <TextField icon='id card outline'
                                                   iconPosition='left'
                                                   placeholder='First name'
                                                   name='firstName'
                                                   label={false}/>
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpProfile.familyName' } defaultMessage={ 'Family name: ' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <TextField icon='id card'
                                                   iconPosition='left'
                                                   action={<ShareWith name='shareWith.familyName'/>}
                                                   placeholder='Family name'
                                                   name='familyName'/>
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpProfile.description' } defaultMessage={ 'Description' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <LongTextField icon='file text outline'
                                                       autoHeight
                                            //iconPosition='left'
                                                       placeholder='Description'
                                                       name='description'/>

                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpProfile.presentLocation' } defaultMessage={ 'Present location: ' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >

                                        <div className="location">
                                            <Location
                                                placeholder="Location"
                                                autocompletionRequest={{types: ["(cities)"]}}
                                                fluid
                                                name='location'

                                            />
                                            <ShareWith name='shareWith.location'/>
                                            <Icon name="marker" className='location-icon'/>
                                        </div>

                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row className="phone">
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpProfile.phoneNumber' } defaultMessage={ 'Phone number: ' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <TextField
                                            icon='mobile'
                                            label={<CountryCodes name="phone.prefix"/>}
                                            labelPosition='left'
                                            iconPosition='left'
                                            action={<ShareWith name='shareWith.phone'/>}
                                            placeholder='Mobile number'
                                            name='phone.number'/>

                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpProfile.birthday' } defaultMessage={ 'Birthday: ' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <DateField icon='birthday'
                                            // action={<ShareWith name='shareWith.birthday'/>}
                                                   placeholder='Birthday'
                                                   name='birthday'/>
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpProfile.gender' } defaultMessage={ 'Gender: ' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <SelectField
                                            options={[{text: 'Male', value: 'Male', icon: 'man'}, {text: 'Female', value: 'Female', icon: 'woman'}]}
                                            placeholder='Gender'
                                            name='gender'/>
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column  >
                                    <ErrorsField textAlign="center" verticalAlign="middle"/>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column >
                                    <Form.Field >
                                        <SubmitField className="fluid" value='Join'/>
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>

                        </Grid>
                    </AutoForm>
                </Segment>
            </div>
        )
    }
}


const SignUpProfileWithTracker = withTracker(({history}) => {
    const handle = Meteor.subscribe('me');
    const user = Meteor.user()
    return {
        loading: !handle.ready(),
        model: user && user.profile && userUpdateProfile.schema.clean(user.profile),
        onSubmitSuccess() {
            history.push('/sign-up/verify')
        },
        onSubmit(model) {
            console.log('model', model)
            return userUpdateProfile.promise(model)
        },
        schema: userUpdateProfile.schema
    }

})(SignUpProfile);
SignUpProfileWithTracker.displayName = 'SignUpProfile'

export default SignUpProfileWithTracker






