import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {withTracker} from 'meteor/react-meteor-data'
import {Form, Grid, Dimmer, Header, Icon, Segment} from "semantic-ui-react";
import {ErrorsField, SubmitField, TextField} from  "uniforms-react-semantic"
import AutoForm from "../../uniforms/AutoForm";
import Action from "../Common/FormAction";
import CountryCodes from "./CountryCodes";
import {userVerifySchema} from "../../../api/Users/Users";
import {ReactiveVar} from 'meteor/reactive-var'
import {FormattedMessage} from 'react-intl'

class SignUpVerify extends Component {
    constructor(props) {
        super(props)
    }

    static propTypes = {
        mobileLeft: PropTypes.number,
        tabletLeft: PropTypes.number,
        computerLeft: PropTypes.number,
        model: PropTypes.object.isRequired
    }
    static defaultProps = {
        mobileLeft: 16,
        tabletLeft: 4,
        computerLeft: 3,
        verified: (active) => (<Dimmer inverted active={active}>
            <Header as='h2' icon>
                <Icon name='check' color="green"/>
                Verified
            </Header>
        </Dimmer>)
    }

    render() {
        const {mobileLeft, tabletLeft, computerLeft, schema, resendPhoneCode, error, onChange, resendEmailCode, onValidate, onSubmit, onSubmitSuccess, onSubmitFailure, model = {}, verified, emailVerified, phoneVerified, ...props} = this.props
        console.log('emailVerified,phoneVerified', emailVerified, phoneVerified)
        const mobileRight = 16, tabletRight = 16 - tabletLeft, computerRight = 16 - computerLeft
        return (
            <div className="SignUpVerify">
                <Segment basic>
                    <Header size='huge'>
                        <FormattedMessage id={ 'Auth.SignUpVerify.header' } defaultMessage={ 'Verify Account' }/>
                    </Header>
                    <AutoForm
                        schema={schema}
                        onValidate={onValidate}
                        onSubmit={onSubmit}
                        onSubmitSuccess={onSubmitSuccess}
                        onSubmitFailure={onSubmitFailure}
                        model={model}
                        onChange={onChange}
                        error={error}
                        showInlineError
                    >
                        <Dimmer.Dimmable as={Grid} dimmed={true}>
                            {verified(emailVerified)}
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpVerify.email' } defaultMessage={ 'Email Address:' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <TextField icon='mail outline'
                                                   iconPosition='left'
                                                   placeholder='Email'
                                                   action={<Action onClick={resendEmailCode} content="Resend code"/>}
                                                   name='email'
                                                   label={false}
                                        />
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpVerify.emailVerificationCode' } defaultMessage={ 'Email verification code:' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field inline>
                                        <TextField icon='barcode'
                                                   iconPosition='left'
                                                   placeholder='Email verification code'
                                                   name='emailCode'
                                                   label={false}
                                        />
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                        </Dimmer.Dimmable>
                        <br/>
                        <Dimmer.Dimmable as={Grid} dimmed={true}>
                            {verified(phoneVerified)}
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpVerify.mobilePhone' } defaultMessage={ 'Mobile phone:' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field >
                                        <TextField
                                            icon='mobile'
                                            label={<CountryCodes name="phone.prefix"/>}
                                            labelPosition='left'
                                            iconPosition='left'
                                            placeholder='Mobile number'
                                            action={<Action onClick={resendPhoneCode} content="Resend code"/>}
                                            name='phone.number'
                                        />
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column mobile={mobileLeft} tablet={tabletLeft} computer={computerLeft}>
                                    <label>
                                        <FormattedMessage id={ 'Auth.SignUpVerify.mobileVerificationCode' } defaultMessage={ 'Mobile verification code:' }/>
                                    </label>
                                </Grid.Column>
                                <Grid.Column mobile={mobileRight} tablet={tabletRight} computer={computerRight}>
                                    <Form.Field inline>
                                        <TextField icon='barcode'
                                                   iconPosition='left'
                                                   placeholder='Email verification code'
                                                   name='phoneCode'
                                                   label={false}
                                        />
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                        </Dimmer.Dimmable>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column  >
                                    <ErrorsField  />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column >
                                    <Form.Field >
                                        <SubmitField className="fluid" value='Verify'/>
                                    </Form.Field>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </AutoForm>
                </Segment>
            </div>
        )
    }
}


const error = new ReactiveVar()


const SignUpVerifyWithTracker = withTracker(({history}) => {
    const handle = Meteor.subscribe('me');
    const user = Meteor.user()
    if (!user) return {loading: true}
    const emailVerified = user && user.emails && user.emails[0] && user.emails[0].verified
    const phoneVerified = user && user.profile && user.profile.phone && user.profile.phone.verified
    if (phoneVerified && emailVerified) history.replace('/welcome')
    return {
        emailVerified,
        phoneVerified,
        loading: !handle.ready(),
        model: {
            email: user && user.emails && user.emails[0] && user.emails[0].address,
            phone: user && user.profile && user.profile.phone,
        },
        onSubmitSuccess() {
            history.replace('/sign-up/verify')
        },
        onSubmitFailure(err) {
            console.log('errerrerrerr', err)
        },
        onSubmit(model) {
            //model = userVerifySchema.clean(model)
            return new Promise((resolve, reject) => {
                Meteor.call('userVerify', model, (err, result) => {
                    if (err) {
                        console.error('userVerify', err)
                        return reject(err)
                    }
                    resolve(result)
                })
            })
        },
        resendPhoneCode() {
            console.log('resendPhoneCode', this)
            const phone = this.findValue('phone')
            return new Promise((resolve, reject) => {
                Meteor.call('userResendPhoneCode', {phone}, (err, result) => {
                    if (err) {
                        reject(err)
                        console.error('userResendPhoneCode', err)
                        //noinspection JSCheckFunctionSignatures
                        error.set({details: [{name: 'phone.number', message: err.reason || err.message}]})
                        return
                    }
                    resolve(result)
                })
            })
        }

        ,
        resendEmailCode() {
            const email = this.findValue('email')
            return new Promise((resolve, reject) => {
                Meteor.call('userResendEmailCode', {email}, (err, result) => {
                    console.log(err, result)
                    if (err) {
                        reject(err)
                        console.error('userResendEmailCode', err)
                        //noinspection JSCheckFunctionSignatures
                        error.set({details: [{name: 'email', message: err.reason || err.message}]})
                        return
                    }
                    resolve(result)
                })
            })
        }

        ,

        schema: userVerifySchema,
        error: error.get(),
        onChange(key, val) {
            let e = error.get()

            if (e && e.details) {
                for (const i in e.details) {
                    if (e.details[i].name === key) {
                        e.details.splice(i, 1)
                    }
                }
                if (e.details.length) {
                    error.set(e)
                } else {
                    error.set(undefined)

                }
            }


        }
    }
})
(SignUpVerify);

SignUpVerifyWithTracker.displayName = 'SignUpVerify'
export default SignUpVerifyWithTracker




