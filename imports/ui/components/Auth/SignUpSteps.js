import React from 'react'
import {Icon, Step} from 'semantic-ui-react'
import PropTypes from 'prop-types'
import { FormattedMessage } from 'react-intl'


const SignUpSteps = (props) => {
    const {step} = props
    return (
        <div>
            <Step.Group fluid stackable="tablet">
                <Step  active={step === 1} disabled={step > 1}>
                    <Icon name='user circle outline'/>
                    <Step.Content>
                        <Step.Title>
                            <FormattedMessage
                              id={ 'Auth.SignUpSteps.createAccount' }
                              defaultMessage={ 'Create Account' }
                            />                                               
                        </Step.Title>
                    </Step.Content>
                </Step>
                <Step active={step === 2} disabled={step > 2}>
                    <Icon name='user'/>
                    <Step.Content>
                        <Step.Title>
                            <FormattedMessage
                              id={ 'Auth.SignUpSteps.create Profile' }
                              defaultMessage={ 'Create Profile' }
                            /> 
                         </Step.Title>
                    </Step.Content>
                </Step>
                <Step active={step === 3} disabled={step > 3}>
                    <Icon name='check circle outline'/>
                    <Step.Content >
                        <Step.Title>
                            <FormattedMessage
                              id={ 'Auth.SignUpSteps.verifyAccount' }
                              defaultMessage={ 'Verify Account' }
                            /> 
                        </Step.Title>
                    </Step.Content>
                </Step>
            </Step.Group>
        </div>
    )
}

export default SignUpSteps

SignUpSteps.defaultProps = {
    step: 2
}
SignUpSteps.propTypes = {
    step: PropTypes.number
}
