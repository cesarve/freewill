/**
 * Created by cesar on 27/9/17.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {withTracker} from 'meteor/react-meteor-data'
import {Grid, Header, Icon, Loader, Segment} from "semantic-ui-react";


class SignUpEmailVerify extends Component {
    state = {loading: true, message: 'Verifying email'}

    componentDidMount() {
        this.props.verify()
            .then((result) => {
                this.setState({loading: false, error: false, message: 'Email Verified'})
            })
            .catch((err) => {
                this.setState({loading: false, error: true, message: err.reason || err.message})
            })
    }

    render() {
        const {loading, message, error} = this.state
        return (
            <Grid>
                <Grid.Row  centered>
                    <Grid.Column>
                        <Header as='h1'>Email verification</Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row >
                    <Grid.Column mobile={16} tablet={8}>
                        <Segment  textAlign='center'>
                            <Loader active={loading}/><br/>
                            {!loading && error && <Icon loading={loading} size="huge" name="warning sign" color="red"/>}
                            {!loading && !error && <Icon loading={loading} size="huge" name="check" color="green"/>}
                            <p>{message}</p>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>


        )
    }

    static propsTypes = {
        verify: PropTypes.func.isRequired
    }
}

export default withTracker(({match}) => {
    const {userId, emailCode} = match.params

    console.log('withTracker SignUpEmailVerify', userId, emailCode)
    return {
        verify(){
            return new Promise((resolve, reject) => {
                Meteor.call('userVerifyEmail', {userId, emailCode}, (err, result) => {
                    if (err) {
                        reject(err)
                        return
                    }
                    resolve(result)
                })
            })
        }
    }
})(SignUpEmailVerify)




