/**
 * Created by cesar on 27/9/17.
 */

import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button, Divider, Header, Icon, Segment} from "semantic-ui-react";
import {withTracker} from 'meteor/react-meteor-data'
import {toPromise} from "../../../api/util";
import SocialConnect from "./SocialConnect";
import {Link} from "react-router-dom";
import SingIn from "./SignIn";


export default class SignInSocialOrEmail extends Component {
    constructor(props) {
        console.log('SignInSocialOrEmail')
        super(props)
    }

    render() {
        return (
            <Segment basic>
                <Header as="h1">Sign In</Header>
                <Segment padded textAlign='center'>
                    <SocialConnect redirectTo="/dashboard" />
                    <Divider horizontal>Or</Divider>
                    <SingIn/>
                </Segment>
            </Segment>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}







