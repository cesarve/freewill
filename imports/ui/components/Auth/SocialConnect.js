import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button, Icon, Segment} from "semantic-ui-react";
import {withTracker} from 'meteor/react-meteor-data'
import {toPromise} from "../../../api/util";
import Errors from '../Common/Errors'
import {withRouter} from "react-router-dom";

class SocialConnect extends Component {
    constructor(props) {
        super(props)
    }

    state = {loading: false}

    login(promise) {
        this.setState({loading: true, errors: undefined})
        promise()
            .then(() => {
                this.setState({loading: false})
            })
            .catch((err) => {
                this.setState({loading: false, errors: err.reason || err.message})
            })
    }

    render() {
        const {loginWithFacebook, loginWithGoogle} = this.props
        const {loading, errors} = this.state
        return (

            <Segment basic loading={loading} textAlign="left">
                <Button color='facebook' size="massive" fluid onClick={() => this.login(loginWithFacebook)}>
                    <Icon name='facebook'/> Facebook
                </Button>
                <br/>
                <br/>
                <Button color='google plus' size="massive" fluid onClick={() => this.login(loginWithGoogle) }>
                    <Icon name='google plus'/> Google Plus
                </Button>
                <Errors errors={errors}/>
            </Segment>
        )
    }

    static defaultProps = {}

    static propTypes = {
        loginWithFacebook: PropTypes.func.isRequired,
        loginWithGoogle: PropTypes.func.isRequired
    }
}


const SocialConnectWithTracker = withRouter(withTracker(({history, redirectTo}) => {
    if (Meteor.userId()) history.replace(redirectTo)
    return {

        loginWithFacebook() {
            return toPromise(Meteor.loginWithFacebook, {
                requestPermissions: ['public_profile', 'email', 'user_about_me', 'user_location', 'user_birthday'],
            })
        },
        loginWithGoogle() {
            return toPromise(Meteor.loginWithFacebook, {
                requestPermissions: ['public_profile', 'email', 'user_about_me', 'user_location', 'user_birthday'],
                redirectUrl: 'http://localhost:3000/_auth/facebook?close',
            })

        },
    }
})(SocialConnect));
SocialConnectWithTracker.displayName = 'SocialConnect'


export default SocialConnectWithTracker
