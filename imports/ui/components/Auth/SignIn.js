import React, {Component}   from 'react';
import PropTypes        from 'prop-types'
import {Form, Grid, Segment}  from 'semantic-ui-react'
import AutoField        from 'uniforms-react-semantic/AutoField';
import AutoForm         from '../../uniforms/AutoForm'
import SubmitField      from 'uniforms-react-semantic/SubmitField';
import {userSignInSchema} from '/imports/api/Users/Users'
import {toPromise} from "../../../api/util";
import {withTracker} from 'meteor/react-meteor-data'
import {ErrorsField} from "uniforms-react-semantic";


export class SingIn extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {onSubmit, schema, model = {}} = this.props
        return (
            <div>
                <Segment stacked>
                    <AutoForm schema={schema} onSubmit={onSubmit} model={model} showInlineError>
                        <Form.Field >
                            <AutoField icon='mail outline' placeholder='Email' name='email'/>
                        </Form.Field>
                        <Form.Field >
                            <AutoField icon='key' placeholder='Password' name='password'
                                       type='password'/>
                        </Form.Field>
                        <Form.Field >
                            <SubmitField className="fluid" value='Submit'/>
                        </Form.Field>
                        <Grid>
                            <Grid.Row>
                                <Grid.Column  >
                                    <ErrorsField textAlign="center" verticalAlign="middle"/>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </AutoForm>
                </Segment>
            </div>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}


const SingInWithTracker = withTracker(({history}) => {
    return {
        onSubmitSuccess () {
            history.replace('/dashboard')
        },
        onSubmit (model) {
            return toPromise(Meteor.loginWithPassword, model.email, model.password)
        },
        schema: userSignInSchema
    }
})(SingIn);
SingInWithTracker.displayName = 'SingIn'

export default SingInWithTracker