/*
 * Created by Jose Carmona on 12-10-2017
 */
import React, {Component} from 'react'
import {List,Header, Container, Segment} from 'semantic-ui-react'
import {FormattedMessage} from 'react-intl';

class Term extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <Container>

                <Header as='h2' textAlign='center' disabled>
                    <FormattedMessage
                        id={ 'Static.Term.header' }
                        defaultMessage={ 'TERMS OF ACCESS' }
                    />
                </Header>

                    <Segment basic  celled='internally' textAlign="center">
                            <p>
                                <FormattedMessage
                                    id={ 'Static.Term.subtitle' }
                                    defaultMessage={ 'To access Freewill and by accessing Freewill you agree:' }
                                />
                            </p>
                    </Segment>

                <List>
                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_1' }
                                defaultMessage={ "1) You will only create an account for your individual self, and you won't provide incorrect account information (name, birth date, gender, contact information) or create more than one Freewill account until such time as duplicate accounts can be identified" }
                            />
                            <List bulleted>
                                <List.Item>
                                    <FormattedMessage
                                        id={ 'Static.Term.item_1.1' }
                                        defaultMessage={ "This enables creators to be able to make clearer true choices (e.g. when choosing who to share provisions with and who to send requests to), for the fair distribution of newly created power (105 each Sunday) and grace (105 each Sunday), and for equitable rights such as voting rights." }
                                    />
                                </List.Item>
                            </List>
                        </p>
                    </List.Item>

                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_2' }
                                defaultMessage={ "2) You won't hold Freewill or its creators responsible or accountable for the use of Freewill by others." }
                            />
                            <List bulleted>
                                <List.Item>
                                    <FormattedMessage
                                        id={ 'Static.Term.item_2.1' }
                                        defaultMessage={ "Freewill is an environment with elements (like our planet Earth, with creative potentials, beings, differences, friction, fire, curiosity, theories, beliefs, language, music, science, reasons, and choices) and has diverse individuals using it, some to survive, some to explore options, and some to consciously create a reality more of our choosing." }
                                    />
                                </List.Item>
                            </List>
                        </p>
                    </List.Item>

                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_3' }
                                defaultMessage={ "3) You won't use Freewill to attempt and/or successfully deceive, bind, control, harm, molest or destroy (violate) any sentient beings without their freely given and continually provided clear consent or against their more recent easy expressions of non-consent, with such expressions of concent and non-consent being made clearly known, enabled and adhered with." }
                            />
                        </p>
                    </List.Item>

                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_4' }
                                defaultMessage={ "4) You won't use Freewill to create, share, buy or sell provisions made from intentionally deceiving, binding, controlling, harming, molesting or destroying (violating) any creators and/or beings without their freely given and continually provided clear consent or against their more recent easy expressions of non-consent, with such expressions of concent and non-consent being made clearly known, enabled and adhered with." }
                            />
                        </p>
                    </List.Item>

                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_5' }
                                defaultMessage={ "5) You won't use Freewill to share pornographic or intentionally obscene provisions until such provisions can be easily identified as such and can easily be filtered by visitors and creators with Freewill." }
                            />
                        </p>
                    </List.Item>
                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_6' }
                                defaultMessage={ "6) You won't endorse other creators who you don't know and haven't shared time with directly. Endorsements are not reviews of provisions, they're endorsements of character." }
                            />
                        </p>
                    </List.Item>
                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_7' }
                                defaultMessage={ "7) You won't trade grace or voting rights. Any agreements to trade grace or voting rights will be void and non-binding upon being formed." }
                            />
                        </p>
                    </List.Item>
                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_8' }
                                defaultMessage={ "8) You won't intentionally deface or damage the presentation or functionality of Freewill, access restricted information that you do not have authorised permission to access, or intentionally and non-consentually disrupt access to Freewill." }
                            />
                        </p>
                    </List.Item>
                    <List.Item>
                        <p>
                            <FormattedMessage
                                id={ 'Static.Term.item_9' }
                                defaultMessage={ "9) If you violate these terms then you will adequately compensate Freewill's effected creators for any material and/or clear quantifiable opportunity costs incurred as a consequence." }
                            />
                            <List bulleted>
                                <List.Item>
                                    <FormattedMessage
                                        id={ 'Static.Term.item_9.1' }
                                        defaultMessage={ "Any adequate compensation to be sought by Freewill for effected creators will be determined by a majority share vote by creators of Freewill who choose to vote for this determination and will be within a 20% deviation of average determined adequate compensation for relevant precedents. Violation of these terms may also result in your Freewill account, grace & power being disabled and/or deleted, as well as legal remedy if so decided with an 85% majority share vote by creators of Freewill who vote for or against such potential responses." }
                                    />
                                </List.Item>
                            </List>
                        </p>
                    </List.Item>
                </List>

            </Container>
        )
    }
}

export default Term