/*
 * Created by Jose Carmona on 12-10-2017
 */
import React, { Component } from 'react'
import { Div, Image, Divider } from 'semantic-ui-react'
import { FormattedMessage } from 'react-intl';

class Sharing extends Component{
    constructor(props) {
        super(props);
    }

    render(){
    	return(
				<div>
				      <Image src={this.props.img} size='tiny' verticalAlign='middle'/>
				      <span>
							<FormattedMessage
                            	id={ 'Static.Sharing.description' }
                                defaultMessage={ '{description}' }
                                values={{ description: this.props.description}}
                        	/>
                            <strong>{this.props.name}</strong>
                            <Divider />
				      </span>						    
				</div> 
			)
    }
}

export default Sharing