/*
 * Created by Jose Carmona on 12-10-2017
 */
import React, { Component } from 'react'
import { Header, Div, Grid, Container, List, Image, Embed } from 'semantic-ui-react'
import { FormattedMessage } from 'react-intl';
import Sharing from './Sharing'

class About extends Component{
    constructor(props) {
        super(props);
    }


    render(){

    	return(
				<div>
					
                    <Header as='h2' textAlign='center' disabled>
                        <FormattedMessage
                            id={ 'Static.About.header' }
                            defaultMessage={ 'ABOUT FREEWILL' }
                        />
                    </Header>
					
					<Grid padded>
	                    <Grid textAlign="center" container>
	                    	<p>
	                    		<FormattedMessage
	                            	id={ 'Static.About.subtitle_1' }
	                            	defaultMessage={ 'Freewill is created by individuals and organisations choosing to create a brighter world by empowering life' }
	                        	/>
	                    	</p>

	                    	<p>
	                    		<strong>
									<FormattedMessage
		                            	id={ 'Static.About.subtitle_2.1' }
		                            	defaultMessage={ 'Our Mission: ' }
	                        		/>
	                    		</strong>

	                    		<FormattedMessage
	                            	id={ 'Static.About.subtitle_2.2' }
	                            	defaultMessage={ 'Ensure we all have enough currency and opportunity to live with freewill' }
	                        	/>
	                    	</p>
	                    </Grid>
	                </Grid>    

                    <Grid container celled='internally'>
                    	<p>
							<FormattedMessage
                            	id={ 'Static.About.content_1' }
                            	defaultMessage={ 'Freewill has been founded in Australia with vegan values of consideration, compassion and love of life. Anyone who creates an account with freewill is a creator.' }
                        	/>
                    	</p>
                    	<p>
							<FormattedMessage
                            	id={ 'Static.About.content_2' }
                            	defaultMessage={ 'We are creating a world where labouring is optional, by selling some of our products and services for power. Power is created from equal empowering payments to all creators and may be traded for these provisions.' }
                        	/>
                    	</p>
                    	<p>
							<FormattedMessage
                            	id={ 'Static.About.content_3' }
                            	defaultMessage={ 'We create the trading value of power by trading such provisions for it, thus increasing the value of one-another’s equal empowering payments and enabling one-another to live more freely and genuinely in the process.' }
                        	/>
                    	</p>
                    	<p>
							<FormattedMessage
                            	id={ 'Static.About.content_4' }
                            	defaultMessage={ 'When all creators are able to live well, solely with the equal empowering payments we’re co-creating, then we will all be freely empowered to live well and to share and sell our labour and provisions with absolute freewill.' }
                        	/>
                    	</p>
                    	<p>
							<FormattedMessage
                            	id={ 'Static.About.content_5' }
                            	defaultMessage={ 'We’re enabling and inspiring one-another to freely share gifts with one-another and to sell labour and provisions because we truly want to, for positive reasons such as a healthy desire to explore, to gain experience, to create extensions of ourselves, to share with others, to gain credibility and access, to co-create projects we want to bring to life, to create a better life for ourselves and others, and even to gain more power without financial coercion, enabling one-another to live well with freewill.' }
                        	/>
                    	</p>

                    	<h4>
		                	<u>
								<FormattedMessage
		                        	id={ 'Static.About.content_6' }
		                        	defaultMessage={ 'We are co-creating a universal income, freeing one-another from forced labour' }
		                    	/>
		                	</u>
		                </h4>

                    	<List bulleted>
						    <List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_1' }
		                            	defaultMessage={ 'All creators gain 1000 power each Sunday' }
		                        	/>
		                    	</p>
						    </List.Item>

 							<List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_2' }
		                            	defaultMessage={ 'We can gain more power by selling products and services' }
		                        	/>
		                    	</p>
						    </List.Item>

 							<List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_3' }
		                            	defaultMessage={ 'We can use our power to purchase products and services from other creators' }
		                        	/>
		                    	</p>
						    </List.Item>

 							<List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_4' }
		                            	defaultMessage={ 'Power incentivises us to sell products and services in ways that others want, while making labour optional' }
		                        	/>
		                    	</p>
						    </List.Item>
                    	</List>
                    </Grid>

					<Grid  centered relaxed columns={2}>
						<Grid.Column>
							<Embed
								id='_EIGfzKvg5I'
								source='youtube'
							  />
						</Grid.Column>
					</Grid>

					<Grid container celled='internally'>
						<h4>
		                	<u>
								<FormattedMessage
	                            	id={ 'Static.About.content_7' }
	                            	defaultMessage={ 'We are also freely sharing gifts with one-another' }
	                        	/>
	                        </u>	
                    	</h4>

                    	<List bulleted>
						    <List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_5' }
		                            	defaultMessage={ 'Creators can also share products and services freely' }
		                        	/>
		                    	</p>
						    </List.Item>
						    <List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_6' }
		                            	defaultMessage={ 'We can each see the percentage of products and services shared freely by other creators and organisations' }
		                        	/>
		                    	</p>
						    </List.Item>
						    <List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_7' }
		                            	defaultMessage={ 'Sharing products and services freely enables us to gain more experience, credability & promotional opportunity, and to freely support one-another' }
		                        	/>
		                    	</p>
						    </List.Item>						    
						</List>
					</Grid>

					<Grid centered relaxed columns={2}>
						<Grid.Column>
							<Embed
								id='EaxjxICgahc'
								source='https://youtu.be/EaxjxICgahc'
							/>
						</Grid.Column>
					</Grid>

					<Grid container>
						<h4>
		                	<u>
								<FormattedMessage
	                            	id={ 'Static.About.content_8' }
	                            	defaultMessage={ 'In addition to enabling us to live freely, freewill also embues us with super human abilities' }
	                        	/>
                    		</u>
                    	</h4>

                    	<List bulleted>
						    <List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_8' }
		                            	defaultMessage={ "We're creating an organised global network, enabling us to share clearer options, make more intelligent connections and to navigate more intentionally" }
		                        	/>
		                    	</p>
						    </List.Item>
						    <List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_9' }
		                            	defaultMessage={ 'Sharing, highlighting and reviewing options enables us to show one-another what matters to us and to share what we think and how we feel about such matters' }
		                        	/>
		                    	</p>
						    </List.Item>
						    <List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_10' }
		                            	defaultMessage={ 'Connecting provisions (e.g. questions, statements, photos, videos) to other provisions (e.g. products, policies, petitions, and news articles) enables us to intelligently expand our awareness' }
		                        	/>
		                    	</p>
						    </List.Item>
						    <List.Item>
								<p>
									<FormattedMessage
		                            	id={ 'Static.About.content.listItem_11' }
		                            	defaultMessage={ 'Identifying ' }
		                        	/>
		                        	<u>	
										<FormattedMessage
			                            	id={ 'Static.About.content.listItem_13' }
			                            	defaultMessage={ 'peer view groups ' }
			                        	/>
			                        </u>
			                        <FormattedMessage
		                            	id={ 'Static.About.content.listItem_14' }
		                            	defaultMessage={ 'of creators enables us to navigate while solely considering their highlights, connections and reviews, enabling us to to sense their perspective and navigate with their support' }
		                        	/>
		                    	</p>
						    </List.Item>
						</List>

						<h4>
		                	<u>
								<FormattedMessage
	                            	id={ 'Static.About.content_9' }
	                            	defaultMessage={ 'Here are some imaginary persons sharing some reasons why creators may enjoy living with Freewill' }
	                        	/>
                    		</u>
                    	</h4>

 						<Sharing img={'img/default-avatar.png'} name={'Sonne Guy'} description={'I enjoy sharing time and provisions freely with people who are clearly choosing to create a free way of life.” ~ Sonne Guy'} />
 						<Sharing img={'img/default-avatar.png'} name={'Anonymous'} description={'Freewill enables me to easily connect with others and to share my time with others in the ways I enjoy most. I like sensing where I’m having a positive effect on the lives of others, while improving myself and developing a clearer identity.'} />
 						<Sharing img={'img/default-avatar.png'} name={'Spirit Guyde'} description={'"I heard some guy say “you can’t serve both god and money”. I`ve thought about these words a fair bit and about the nature of "service" representing obedience and/or an agreed provision of something for remuneration, and I really like how Freewill enables us to create a free way of living so we may freely choose whether, when, where, who, why and how to serve with clear options, choices and reasoning.'} />
 						<Sharing img={'img/default-avatar.png'} name={'Nine Often'} description={'I appreciate Freewill enabling us to share, hilight and review provisions and to clearly sense and explore provisions that have been shared, hilighted and reviewed; This makes it easier to identify what provisions matter most to me, how I feel about them and why, and to sense what matter most to others, how they feel about them and why. The "Peer View" option enables us to identify peers & just consider their expressions when navigating (e.g. family and friends, living in a particular space&time, of a specific gender and age range, who my reviews and votes clearly align with).'} />

					</Grid>
                </div>
    	)
    }

}

export default About