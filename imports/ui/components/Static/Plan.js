/*
 * Created by Jose Carmona on 12-10-2017
 */
import React, { Component } from 'react'
import { Header, Grid } from 'semantic-ui-react'
import { FormattedMessage } from 'react-intl';

class Plan extends Component{
    constructor(props) {
        super(props);
    }

    render(){

    	return(
				<div>
                    <Header as='h2' textAlign='center' disabled>
                        <FormattedMessage
                            id={ 'Static.Plan.header' }
                            defaultMessage={ '¡COMMING SOON!' }
                        />
                    </Header>
                </div>
    		)
	}

}

export default Plan