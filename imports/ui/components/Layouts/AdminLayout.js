import React from 'react';
import {Component} from 'react'
import {Container, Segment} from 'semantic-ui-react'
import Menu from './Menu/Menu'
import Header from '../Header/Header'
import Footer from "../Home/Footer";
//import './mainlayout.css'

/**
 *         <div className="leftLayout">
 <Menu />
 </div>
 * Main Layout
 */
export class MainLayout extends Component {
    constructor(props) {
        super(props)
    }

    render() {

        return (
            <div>
                <Header/>

                <div  className="rightLayout">
                    {this.props.children}
                </div>
                <Footer/>
            </div>

        )
    }
}
MainLayout.defaultProps = {}

MainLayout.propTypes = {}

export default MainLayout