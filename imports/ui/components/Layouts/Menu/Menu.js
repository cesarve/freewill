import React from 'react'
import {Component} from 'react'
import PropTypes from 'prop-types'
import {Menu, Icon} from 'semantic-ui-react'
import {} from 'react-intl'
import './menu.css'
import {Link,} from 'react-router-dom'
import {FormattedMessage} from "react-intl";


class MainMenu extends Component {
    render() {
        const {show} = this.props
        const title = show === 'title' || show === 'both'
        const icon = icon === 'icon' || show === 'both'
        return (
            <div className="mainMenu">
                <Menu vertical icon='labeled'>
                    <Menu.Item as={Link} name='home' to="/" link active>
                        {icon && <Icon name='home'/>}
                        {title && <FormattedMessage
                            id={ 'Layouts.Menu.home' }
                            defaultMessage={ 'Home' }
                        />}
                    </Menu.Item>
                    <Menu.Item as={Link} name='creators' to="/" link>
                        {icon && <Icon name='user'/>}
                        {title && <FormattedMessage
                            id={ 'Layouts.Menu.creators' }
                            defaultMessage={ 'Creators' }
                        /> }
                    </Menu.Item>
                    <Menu.Item as={Link} name='abilities' to="/" link>
                        {icon && <Icon name='wait'/>}
                        {title && <FormattedMessage
                            id={ 'Layouts.Menu.abilities' }
                            defaultMessage={ 'Abilities' }
                        /> }
                    </Menu.Item>
                    <Menu.Item as={Link} name='products' to="/" link>
                        {icon && <Icon name='gift'/>}
                        {title && <FormattedMessage
                            id={ 'Layouts.Menu.products' }
                            defaultMessage={ 'Products' }
                        /> }
                    </Menu.Item>
                    <Menu.Item as={Link} name='organizations' to="/" link>
                        {icon && <Icon name='building outline'/>}
                        {title && <FormattedMessage
                            id={ 'Layouts.Menu.organizations' }
                            defaultMessage={ 'Organizations' }
                        /> }
                    </Menu.Item>
                </Menu>
            </div>
        )
    }

    static defaultProps = {
        /**
         * @type: {string} show icon | both | title in menu items
         * @default: true
         */
        show: 'title',
    }
    static propTypes = {
        show: PropTypes.oneOf(['icon', 'title', 'both']),
    }
}
export default MainMenu