import React from 'react'
import {Component} from 'react'
import PropTypes from 'prop-types'
import {Menu, Icon} from 'semantic-ui-react'
import {} from 'react-intl'
import './menu.css'
import {Link,} from 'react-router-dom'


class AdminMenu extends Component {
    render() {
        const {show} = this.props
        const title = show === 'title' || show === 'both'
        const icon = icon === 'icon' || show === 'both'
        return (
            <div className="mainMenu">
                <Menu icon='labeled'>
                    <Menu.Item as={Link} name='home' to="/admin/users" link active>
                        {icon && <Icon name='users'/>}
                        {title && 'Users'}
                    </Menu.Item>
                </Menu>
            </div>
        )
    }

    static defaultProps = {
        /**
         * @type: {string} show icon | both | title in menu items
         * @default: true
         */
        show: 'title',
    }
    static propTypes = {
        show: PropTypes.oneOf(['icon', 'title', 'both']),
    }
}
export default AdminMenu