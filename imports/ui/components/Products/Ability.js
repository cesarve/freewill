/**
 * Created by Jose Carmona on 20/09/17.
 */
import React , {Component} from 'react'
import {Image, Grid} from 'semantic-ui-react'

//only test
import { addLocaleData , IntlProvider, FormattedMessage , FormattedDate } from 'react-intl';
import en from 'react-intl/locale-data/en';
import es from 'react-intl/locale-data/es';

import localeData from './data.json';

addLocaleData([...en, ...es]);

const language = navigator.languages;
const messages = localeData[language] || localeData.en;


class Ability extends Component{
	constructor (props) {
		super(props);
	}

	render(){
		return(
			<Grid columns='equal'>
                <Grid.Row>
                    <Grid.Column  width={16}>					
                    	<h5>
							<FormattedMessage
							  id={ 'Products.Ability.ability' }
							  defaultMessage={ 'Ability: {ability} ' }
							  values={{ ability: this.props.ability }}
							/>	
						</h5>
					</Grid.Column>
                	<Grid.Column  width={16}>
						<Image src={this.props.img} avatar />
						<label>
						
							<FormattedMessage
							  id={ 'Products.Ability.Provided' }
							  defaultMessage={ 'Provided by ' }
							/>

							{this.props.name} 

							<FormattedMessage
							 id={'Products.Ability.Level'}
							 defaultMessage={'{level}'}
							 values={{ level: this.props.level }}
							/>

						</label>
					</Grid.Column>
				</Grid.Row> 
			</Grid>
		)
	}
}

export default Ability