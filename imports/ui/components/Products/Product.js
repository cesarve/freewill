/**
 * Created by Jose Carmona on 20/09/17.
 */

import {Button, Divider, Segment, Header , Image, Div, Grid, Form, TextArea, Select, options} from 'semantic-ui-react'
import React , { Component, propTypes } from 'react'
import AutoForm from  "../../uniforms/AutoForm"
import AutoField from 'uniforms-react-semantic/AutoField'
import TextField from 'uniforms-react-semantic/TextField'
import SubmitField from 'uniforms-react-semantic/SubmitField'
import ErrorsField from 'uniforms-react-semantic/ErrorsField'
import SelectField from 'uniforms-react-semantic/SelectField'
import LongTextField from 'uniforms-react-semantic/LongTextField'
import Ability from './Ability'
import SimpleSchema from 'simpl-schema'
import { FormattedMessage, FormattedDate, injectIntl } from 'react-intl';
import PropTypes from 'prop-types'
import {schema} from './../../../api/Product/productSchema'

/**
 * Render Product Form
 */
class Product extends Component {
	constructor(props) {
        super(props);
	}

	static propTypes = {
		intl: PropTypes.object.isRequired,
	}

	static defaultProps = {
		name: 'kirk',
		name2: 'freewilltesting1',
		location : 'Filipinas ',
        percent1 : '0',
        percent2 : '0',
        requests : '2',	
        level: 'Novice',
        img1: '/images/imagen1.png',
        img2:'img/default-avatar.png',
        ability: 'Massage',
	}

    render() {
    	const { formatMessage } = this.props.intl;
        return ( 
            <div>
                <Segment stacked>                          
                	<AutoForm schema={schema} onSubmit={this.handleSubmit} model={{}} showInlineError>
                		
                		<Header  as='h2' textAlign='center' disabled>
							<FormattedMessage
							  id={ 'Products.Product.header' }
							  defaultMessage={ 'Send a request to {name} freewilltesting2' }
							  values={{ name: this.props.name }}
							/>											
						</Header>
						
						<Segment clearing>
							<Grid container>
								<Grid.Column width={3}>
									<Image src={this.props.img1} size='small' />
								</Grid.Column>
								<Grid.Column width={4}>
									<Ability ability={this.props.ability} img={this.props.img2} name={this.props.name2} level= {this.props.level}/>
								</Grid.Column>
							</Grid>
						</Segment> 

						<Grid padded textAlign='center'>
							<span>
								<FormattedMessage
								  id={ 'Products.Product.location' }
								  defaultMessage={ 'Location: {location}' }
								  values={{ location: this.props.location }}
								/>	
							</span>
							<span>
								<FormattedMessage
								  id={ 'Products.Product.locationConfirmed' }
								  defaultMessage={ '(address will be confirmed if this request is accepted)' }
								  values={{}}
								/>	
							</span>						
						</Grid>

						<Grid columns='equal'>
							<Grid.Row>
								<Grid.Column>
									<Form.Field >
										<TextField placeholder=' 15/03/17' name='shareTime'/>
									</Form.Field >
						        </Grid.Column>
						        <Grid.Column>
						        	<Form.Field >
										<TextField placeholder='11:00 am' name='from'/>
									</Form.Field >
						        </Grid.Column>
						        <Grid.Column>
						        	<Form.Field >
										<TextField placeholder='11:15 am' name='to'/>
									</Form.Field >
						        </Grid.Column>
							</Grid.Row>	
						
							<Grid.Row>
								<Grid.Column>
						            <Form.Field >
										<SelectField name='notice.Required' fluid selection placeholder='1 Day'/>
						             </Form.Field >
						        </Grid.Column>
						        <Grid.Column>
						            <Form.Field >
										<SelectField name='notice.Hours' fluid selection placeholder='0 Hours'/>
						             </Form.Field >
						        </Grid.Column>
						        <Grid.Column>
						            (GMT 10) BY: 11:00AM, 14 MAR 2017
						        </Grid.Column>
						        
							</Grid.Row>	
						
							<Grid.Row>
								<Grid.Column>
						            <Form.Field >
										<SelectField name='requestFor' fluid selection placeholder='Free'/>
						            </Form.Field >
						        </Grid.Column>
						        <Grid.Column>
						            
						        </Grid.Column>
						        <Grid.Column>
						            
						        </Grid.Column>
							</Grid.Row>	
						

					       <Grid.Row>
		                        <Grid.Column>
		                        	<Form.Field >
                                        <LongTextField icon='file text outline'
                                           autoHeight
                                           placeholder="Optionally provide more sense of who you are, any requirements you have, and why you're requesting acces to this ability..."
                                           name='message'/>
                                    </Form.Field>									
									                       								
		                        </Grid.Column>
		                    </Grid.Row>
					</Grid>

					<Divider />

					  <Grid padded textAlign='center'>
					    <Button  color='orange'>Send</Button>
					    <Button  color='grey'>Cancel</Button>
					  </Grid>

					  <Grid textAlign='center'>
					  	<h5>
					  		<FormattedMessage
							  id={ 'Products.Product.Percentage' }
							  defaultMessage={ 'Percentage of requests accepted by' }
							/>					  	 
					  		{this.props.name}: {this.props.percent1}%{this.props.percent2}
					  	</h5>
					  </Grid>
					  <Grid textAlign='center'>
					  	<h5>
					  		<FormattedMessage
							  id={ 'Products.Product.footerLabel2' }
							  defaultMessage={ '(Overlapping with your request: {requests})' }
							  values={{ requests: this.props.requests }}
							/>
					  	</h5>
					  </Grid>
					</AutoForm>  
                </Segment>
            </div>
        
        )
    }
}

export default injectIntl(Product);