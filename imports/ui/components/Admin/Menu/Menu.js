/*
 * Created by Jose Carmona on 12-10-2017
 */
import React, {Component} from 'react'
import {Menu, Tab, Div, Advertisement} from 'semantic-ui-react'
import {FormattedMessage} from 'react-intl';
import AutoTableTranslation from '../../../../api/TableTranslation/AutoTableTranslation'
import LenguajeTab from './../Lenguaje_Tab/LenguajeTab'
import 'react-table/react-table.css'

class SideMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const {tableData, ...props} = this.props

        const panes = [
            {
                menuItem: 'Dashboard', render: () => <Tab.Pane>
                <Advertisement unit='netboard' test='Welcome'/>
            </Tab.Pane>
            },

            {
                menuItem: 'Text', render: () => <Tab.Pane>
                    <AutoTableTranslation />
            </Tab.Pane>
            },

            {menuItem: 'Creators', render: () => <Tab.Pane> </Tab.Pane>},
            {menuItem: 'Products', render: () => <Tab.Pane> </Tab.Pane>},
            {menuItem: 'Services', render: () => <Tab.Pane> </Tab.Pane>},
            {menuItem: 'Organisations', render: () => <Tab.Pane> </Tab.Pane>},
            {menuItem: 'Power', render: () => <Tab.Pane> </Tab.Pane>},
            {menuItem: 'Email events', render: () => <Tab.Pane> </Tab.Pane>},
            {menuItem: 'Notice events', render: () => <Tab.Pane> </Tab.Pane>},
            {menuItem: 'Spam', render: () => <Tab.Pane> </Tab.Pane>},
        ]
        return (
            <Tab panes={panes} menu={{attached: true, pointing: true, vertical: true}}/>
        )
    }
}

export default SideMenu




