import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button, Image, Reveal, Icon} from "semantic-ui-react";
import {userAvatarUpdateStatus} from '/imports/api/Users/users-methods'

const UserActionAvatar = ({value: avatar, original: {_id: userId}}) => {
    if (!avatar || !avatar.url) return null
    return (
        <div className="inside-buttons">
            <Image src={avatar.url}/>
            {avatar.status === 'Approved' && <Icon color='green' name="check"/>}
            {avatar.status === 'Declined' && <Icon color='red' name="ban"/>}
            {avatar.status !== 'Approved' && avatar.status !== 'Declined' && <Button.Group>
                <Button color="green" icon onClick={() => userAvatarUpdateStatus.call({userId, status: 'Approved'})}><Icon name="check"/></Button>
                <Button color="red" icon onClick={() => userAvatarUpdateStatus.call({userId, status: 'Declined'})}><Icon name="ban"/></Button>
            </Button.Group>}
        </div>

    )
}

UserActionAvatar.displayName = 'UserActionAvatar'

export default UserActionAvatar