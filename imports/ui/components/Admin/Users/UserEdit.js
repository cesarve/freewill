import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button, Dimmer, Image, Modal} from "semantic-ui-react";
import {Link, Route, Redirect, withRouter} from "react-router-dom";
import {withTracker} from 'meteor/react-meteor-data'
import AutoForm from "../../../uniforms/AutoForm";
import {userUpdateStatus} from '/imports/api/Users/users-methods'
import SimpleSchema from 'simpl-schema'
import {HiddenField, TextField, SelectField, ErrorsField} from 'uniforms-react-semantic'
import DisplayIf from "../../../uniforms/DisplayIf";
import Reasons from '/imports/api/Reasons/Reasons'
import ModalRoute from '/imports/ui/components/Common/ModalRoute'
import {insertReason} from "../../../../api/Reasons/reasons-methods";
class UserEdit extends Component {
    constructor(props) {
        super(props)
    }

    content = () => {
        const {user, history, reasons, insertReason, schema, onSubmit, loadingReasons} = this.props
        if (!user) return <Redirect to="/404" push/>
        const {_id: userId, username, profile = {}} = user
        const {status, reason, avatar: {url: image = false} = {}} = profile
        console.log('user', user)
        console.log('image', image)
        return (
            <div>
                {image && <Image rounded wrapped size='mini' src={image}/>} {' '} {username}
                <Modal.Description>
                    <AutoForm
                        onSubmit={onSubmit}
                        onSubmitSuccess={() => history.push(`/admin/users`)}
                        ref={(me) => this.form = me}
                        schema={schema}
                        model={{status, userId, reason}}
                    >
                        <HiddenField name="userId"/>
                        <SelectField name="status" options={[
                            {text: 'Pending', value: 'Pending', disabled: true},
                            {text: 'Enabled', value: 'Enabled'},
                            {text: 'Suspended', value: 'Suspended'}
                        ]}/>
                        <DisplayIf condition={context => context.model.status === 'Suspended'}>
                            <SelectField loading={loadingReasons} onAddItem={insertReason} additionPosition="bottom" selection search allowAdditions fluid name="reason" label={false}
                                         options={reasons}>
                            </SelectField>
                        </DisplayIf>
                        <ErrorsField />
                    </AutoForm>
                </Modal.Description>

            </div>
        )
    }

    render() {
        const {loading} = this.props
        return (
            <ModalRoute header="Edit User" loading={loading} onAction={() => this.form.submit()}>
                {!loading && this.content()}
            </ModalRoute>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}


const UserEditWithTracker = withRouter(
    withTracker(({match, history, location}) => {
            const userId = match.params.userId
            const handleReasons = Meteor.subscribe('reasons')
            console.log('userId', userId)
            const handle = Meteor.subscribe('user', userId)
            const loadingReasons = !handleReasons.ready()
            const loading = !handle.ready()
            let user, reasons
            if (!loading) {
                user = Meteor.users.findOne(userId)
            }
            if (!loadingReasons) {
                reasons = Reasons.find().map((reason) => {
                    return {
                        key: reason._id,
                        value: reason.reason,
                        text: reason.reason,
                    }
                })
            }
            return {
                reasons,
                loading,
                loadingReasons,
                user,
                history,
                schema: userUpdateStatus.schema,
                onSubmit: (model) => userUpdateStatus.promise(model),
                insertReason: (e, {value: reason}) => insertReason.call({reason}),

            }
        }
    )(UserEdit)
)

UserEditWithTracker.displayName = 'UserEdit'

export default UserEditWithTracker
