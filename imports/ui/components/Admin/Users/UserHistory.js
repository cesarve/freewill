import React, {Component} from 'react';
import PropTypes from 'prop-types'
import { withRouter} from "react-router-dom";
import {withTracker} from 'meteor/react-meteor-data'

class UserEditHistory extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>UserEditHistory
            </div>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}

const UserEditHistoryWithTracker = withRouter(
    withTracker(({match}) => {
            const userId = match.params.userId
            const handle = Meteor.subscribe('userHistory', userId)
            const loading = !handle.ready()
            let user
            if (!loading) {
                user = user.findOne(Id)
            }
            return {
                loading,
                user
            }
        }
    )(UserEditHistory)
)

UserEditHistoryWithTracker.displayName='UserEditHistory'

export default UserEditHistoryWithTracker
