import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button, Form, Modal} from "semantic-ui-react";
import AutoForm from "../../../uniforms/AutoForm";
import {SelectField, HiddenField, ErrorsField} from "uniforms-react-semantic";
import {userSuspend} from '/imports/api/Users/users-methods'
import {withTracker} from 'meteor/react-meteor-data'
import {withRouter} from "react-router-dom";
import Reasons from '/imports/api/Reasons/Reasons'
import {insertReason} from '/imports/api/Reasons/reasons-methods'
class UserActionSuspend extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        let formRef
        const {loading, schema, model, reasons, insertReason, onSubmit, onCancel, onSubmitSuccess} = this.props
        return (
            <Modal closeOnEscape={true} closeOnRootNodeClick={true} size="small" open>
                <Modal.Header>
                   Provide a reason
                </Modal.Header>
                <Modal.Content >
                    <AutoForm ref={(form) => formRef = form} schema={schema} model={model} onSubmit={onSubmit}
                              onSubmitSuccess={onSubmitSuccess}
                    >
                        <Modal.Content >
                            <Form.Field>
                                <SelectField loading={loading} onAddItem={insertReason} additionPosition="bottom" selection search allowAdditions fluid name="reason" label={false} options={reasons}>
                                </SelectField>
                            </Form.Field>
                            <HiddenField name="userId"/>
                            <ErrorsField/>
                        </Modal.Content>
                    </AutoForm>
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={onCancel}>
                        Cancel
                    </Button>
                    <Button color="red" onClick={() => formRef.submit()}>
                        Suspend
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}

const UserActionSuspendWithTracker = withRouter(
    withTracker(({history, match}) => {
        const handle = Meteor.subscribe('reasons')
        const loading = !handle.ready()
        const userId = match.params.userId
        let reasons = []
        if (!loading) {
            reasons = Reasons.find().map((reason) => {
                return {
                    key: reason._id,
                    value: reason.reason,
                    text: reason.reason,
                }
            })
        }
        return {
            loading,
            schema: userSuspend.schema,
            onSubmit: userSuspend.promise,
            onSubmitSuccess: () => history.push('/admin/users'),
            onCancel: () => history.push('/admin/users'),
            insertReason: (e, {value: reason}) => insertReason.call({reason}),
            reasons,
            model: {userId, reason: ''}
        }
    })(UserActionSuspend)
)

UserActionSuspendWithTracker.displayName = 'UserActionSuspend'

export default UserActionSuspendWithTracker
