import React, {Component} from 'react';
import PropTypes from 'prop-types'
import Users from '/imports/api/Users/UsersAutoTable'
import {Header} from "semantic-ui-react";
import 'react-table/react-table.css'
import './users.less'
class AdminUsers extends Component {

    render() {
        return (
            <div>
                <Header as="h1">Users List</Header>
                <Users/>
            </div>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}


export default AdminUsers
