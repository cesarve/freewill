import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Button, Icon} from "semantic-ui-react";
import {withTracker} from 'meteor/react-meteor-data'
import {Link} from "react-router-dom";


const UserActions = ({value: userId}) => (
    <Button.Group size="mini">
        <Button  size="mini" as={Link} to={`/admin/users/${userId}/edit`}><Icon name="pencil"/></Button>
        <Button  size="mini" as={Link} to={`/admin/users/${userId}/history`}><Icon name="history"/></Button>
    </Button.Group>
)
UserActions.displayName = 'UserActions'

export default UserActions