import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {Route} from 'react-router-dom'
import {Loader, Segment} from 'semantic-ui-react'
import Error from './Error'

class LazyLoaderRoute extends Component {
    render() {
        const {from, render, error, loading, ...props} = this.props
        if (Meteor.isDevelopment) {
            return <Route {...props} render={(props) => <LazyLoader from={from} render={render} loading={loading} error={error} {...props}/>}/>
        }
        return <Route {...props} render={(props) => <LazyLoader from={from} render={render} loading={loading} error={error} {...props}/>}/>
    }
}

export class LazyLoader extends Component {
    state = {
        Component: null
    }

    componentDidMount() {
        this.load(this.props)
    }

    load = (props) => {
        this.setState({
            Component: null
        })
        const {Error, from, render = 'default'} = props
        import(from).then((mod) => {
            const component = mod[render]

            this.setState({Component: component})
        }).catch((err) => {
            if (err.message.indexOf('Cannot find module') >= 0) console.error('Remember add the compomento to /imports/ui/importer')
            console.log(err)
            this.setState({Component: () => Error(err)})
        })

    }

    static dimension(val) {
        if (typeof val === 'string') return val
        return `${val}px`
    }

    render() {
        const {Component} = this.state
        //noinspection JSUnusedLocalSymbols
        const {Error, Loader, from, render, height, width, ...props} = this.props
        return Component ? <Component {...props}/> : (
            <Segment basic style={{height: `${LazyLoader.dimension(height)}`, width: `${LazyLoader.dimension(width)}`}}>
                <Loader active inline="centered" size="massive"/>
            </Segment>
        )
    }

    static defaultProps = {
        height: 200,
        width: "100%",
    }
}


LazyLoaderRoute.defaultProps = {
    Loader: Loader,
    Error: Error
}
LazyLoader.defaultProps = LazyLoaderRoute.defaultProps

/*
 todo
 LazyLoaderRoute.propTypes = {
 from: PropTypes.string.isRequired,
 render: PropTypes.string,
 loading: PropTypes.element,
 error: PropTypes.element,
 }
 LazyLoader.propTypes=LazyLoaderRoute.propTypes
 */

export default LazyLoaderRoute

/*

 if (this.state.component) return <this.state.component/>
 const {from,render,error}=this.props
 console.log('load')


 console.log('this.state.component',this.state.component)
 return <this.state.component/>
 */