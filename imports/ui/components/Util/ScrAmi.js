/**
 * Created by cesar on 12/10/17.
 */

/**
 *
 */
export default class ScrAmi {
    /**
     *
     * @param {DOM} elem - parent element
     * @param {number} start default
     * @param {number} from
     * @param {number} end
     * @param {number} to
     * @param {number} inc
     */
    constructor(elem, {start = 1, from, end = document.scrollingElement.scrollTopMax || document.scrollingElement.scrollHeight || document.documentElement.clientHeight, to, inc} = {}) {
        this._start = parseFloat(start)
        this._from = parseFloat(from)
        this._end = parseFloat(end)
        this._to = parseFloat(to)
        this._inc = parseFloat(inc)
        if (isNaN(this._to)) this._to = undefined
        if (isNaN(this._inc)) this._inc = undefined
        if (isNaN(this._from)) this._from = undefined
        this._elems = elem.querySelectorAll(`[data-scrami-to], [data-scrami-inc]`)
        this.init()
    }

    create = () => {
        window.addEventListener('scroll', this.onScroll)
    }

    destroy = () => {
        window.removeEventListener('scroll', this.onScroll)
    }

    init = () => {
        this.elems = []
        this._elems.forEach((elem) => {
            const properties = (elem.dataset.scramiProperty || 'top').split('|')
            const starts = (elem.dataset.scramiStart || '').split('|')
            const ends = (elem.dataset.scramiEnd || '').split('|')
            const tos = (elem.dataset.scramiTo || '').split('|')
            const incs = (elem.dataset.scramiInc || '').split('|')
            properties.forEach((property, i) => {
                const computedStyle = window.getComputedStyle(elem)[property]
                const from = this._from || parseFloat(computedStyle)
                const matchUnits = /[^0-9]{1,4}/.exec(computedStyle)
                const unit = matchUnits && matchUnits[0];
                if (isNaN(from))  throw new ScrAmiError(property, elem, `Property "${property}" is not numeric (only numeric countable properties are accepted`)
                if (from !== 0 && !from) throw new ScrAmiError(property, elem, `Property "${property}" not found`)
                const start = parseFloat(starts[i])
                const end = parseFloat(ends[i])
                const to = parseFloat(tos[i])
                const inc = parseFloat(incs[i])
                this.elems.push({
                    elem,
                    property,
                    start: !isNaN(start) ? start : this._start,
                    end: !isNaN(end) ? end : this._end,
                    to: !isNaN(to) ? to : !isNaN(inc) ? (from + inc) : this._to,
                    from,
                    unit,
                })
            })
        })
    }
    timeoutId = null
    onScroll = () => {
        for (const elem of this.elems) {
            let y = window.scrollY
            if (y < elem.start) y = elem.start
            if (y > elem.end) y = elem.end
            const scroll = elem.start - elem.end
            const diff = elem.to - elem.from
            const ratio = diff / scroll
            let result
            if (elem.unit) {
                result = `${elem.from - ((y - elem.start) * ratio)}${elem.unit}`
            } else {
                result = elem.from - ((y - elem.start) * ratio)
            }
            elem.elem.style[elem.property] = result
        }
    }

}

class ScrAmiError extends Error {
    constructor(property, element, ...args) {
        super(...args)
        this.property = property;
        this.element = element;
    }
}