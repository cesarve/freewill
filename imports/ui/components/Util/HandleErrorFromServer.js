/**
 * Created by cesar on 29/8/17.
 */
import  React, {Component} from "react";
import PropTypes  from "prop-types";
import _ from 'underscore'

export default class HandleErrorFromServer extends Component {

    constructor(props) {
        super(props)
        this.state = {error: null}
        this.onSubmitSuccess = this.onSubmitSuccess.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.onSubmitFailure = this.onSubmitFailure.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    onSubmit(model, a, b) {
        console.error('Configure onSubmit method in', this)
        if (typeof this.props.onSubmit === 'function') return this.props.onSubmit()
    }

    onSubmitFailure(err) {
        console.log('onSubmitFailure',err)
        //todo maybe do a function for recognize any word in the reason || message and try to attached to the field
        if (err.reason === "Email already exists.") {
            this.setState({error: {details: [{name: 'email', message: err.reason}]}})
        } else {
            this.setState({error: {details: [{message: err.reason || err.message}]}})
        }
        if (typeof this.props.onSubmitFailure === 'function') return this.props.onSubmitFailure(err)
    }

    onSubmitSuccess() {
        this.setState({error: null})
        if (typeof this.props.onSubmitSuccess === 'function') return this.props.onSubmitSuccess()
        //todo notification
        //todo redirect
    }

    onChange(key, value) {
        let errors = this.state.error && this.state.error.details
        if (!errors) return
        let stayErrors = _.reject(errors, e => e.name === key)
        if (stayErrors.length === errors.length) return stayErrors = [] //if are the same erros is because the errors comes from sever
        this.setState({error: stayErrors.length ? stayErrors : null})
        if (typeof this.props.onChange === 'function') return this.props.onChange(key, value)
    }

    render() {
        console.error('Configure render method in', this)
        return null
    }

    static propTypes = {
        onChange: PropTypes.func,
        onSubmitSuccess: PropTypes.func,
        onSubmitFailure: PropTypes.func,
        onSubmit: PropTypes.func,
    }

}