import React from 'react';
import {Component} from 'react'
import PropTypes from 'prop-types'
import SearchType from './SearchType'
import SearchInput from  './SearchInput'

/**
 * Search of header for Main Layout
 */



 const optionsType = [
    {
        text: 'Creators',
        value: 'Creators',
        icon: 'user',
    },
    {
        text: 'Abilities',
        value: 'Abilities',
        icon: 'wait',
    },
    {
        text: 'Products',
        value: 'Products',
        icon: 'gift',
    },
    {
        text: 'Organizations',
        value: 'Organizations',
        icon: 'users',
    },
]

 const optionsResult=[
    {
        text: 'Gold Coast (5)',
        value: ' Gold Coast',
    },

]


class Search extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className="ui grid">
                <div className="row">
                    <div className="five wide column" style={{paddingRight: 0}}>
                        <SearchType options={optionsType}/>
                    </div>
                    <div className="eleven wide column" style={{paddingLeft: 0}}>
                        <SearchInput options={optionsResult} />
                    </div>
                </div>

            </div>
        )
    }
}

Search.defaultProps = {}

Search.propTypes = {}

export default Search
