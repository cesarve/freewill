import React from 'react';
import {Component} from 'react'
import PropTypes from 'prop-types'
import {Dropdown} from 'semantic-ui-react'
import './searchtype.css'
//http://en.blog.koba04.com/2015/06/28/esdoc-documentation-for-react-and-es6/
/**
 * Main Header search component
 * Dropdown to select search type and an input for free textr
 */
export class    SearchType extends Component {
    /**
     * constructor
     * @param props
     */
    constructor(props) {
        super(props)
        const option = this._getOption(0)
        /**
         * @type {object}
         * @property {string,number} search type
         * @property {Component} Icon  - Text to show as place holder
         */
        this.state = {
            value: props.options[0] && props.options[0].value,
            text: option.text,
            icon: option.icon
        }
    }


    /**
     * Handle change: excecuting props.onChange(option value)
     * @param e // React Event of Dropdown Sematic UI https://react.semantic-ui.com/modules/dropdown
     * @param data // all properties for Dropdown Sematic UI https://react.semantic-ui.com/modules/dropdown
     * @returns {*} Value for selected option
     */
    handleChange = (e, data) => {
        const index = this._getIndex(data.value)
        const option = this._getOption(index)
        this.setState({value: data.value, text: option.text, icon: option.icon})
        this.props.onChange(data.value)
    }
    /**
     * @param index
     * @returns {*}
     *
     * Get the option index and return The Icon - Text for the options
     */
    _getOption = (index) => {
        if (index < 0) return {}
        const option = this.props.options[index]
        console.log('option',option)
        return option ? option : {}
    }

    /**
     * get the an option value and return index in options array
     * @param value
     * @returns {number}
     */
    _getIndex(value) {
        const {options}=this.props
        const len = options.length
        for (let index = 0; index < len; index++) {
            if (options[index].value === value) return index
        }
        return -1
    }

    /**
     * Return a Semantic UI  Dropdown
     * @returns {Component}
     */
    render() {
        const {options}=this.props
        const {text, value, icon}=this.state
        return (
            <Dropdown inline fluid  text={text} icon={icon} floating labeled button className='icon searchType' options={options}
                      onChange={this.handleChange} />
        )
    }
}

SearchType.defaultProps = {
    onChange: () => {
    },
    options: [],
}


SearchType.propTypes = {
    /**
     * @type {array}
     * Options array (DropdowItem)
     * [{
     *      text,
     *      value,
     *      icon,
     * }...]
     */
    options: PropTypes.arrayOf(
        PropTypes.shape({
            text: PropTypes.string.isRequired,
            value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
            icon: PropTypes.string.isRequired,
        }).isRequired
    ).isRequired,
    /**
     * @type {function}
     */
    onChange: PropTypes.func
}


export default SearchType