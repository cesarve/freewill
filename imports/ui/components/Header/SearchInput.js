import React, {Component} from 'react';
import PropTypes from 'prop-types'
    import {Dropdown} from 'semantic-ui-react'
import './searchinput.css'

/**
 * Search text Input for header in Main  Layout
 */
class SearchInput extends Component {
    render() {
        return (
            <Dropdown fluid inline search selection  {...this.props} className='searchInput'/>
        )
    }
}

SearchInput.propTypes={
    options: PropTypes.array.isRequired
}

export default SearchInput