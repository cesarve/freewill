import React from 'react';
import {Component} from 'react'
import PropTypes from 'prop-types'
import {Image} from 'semantic-ui-react'

/**
 * Logo for Main Layout
 */
class Logo extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {src} = this.props
        return (
            <div>
                <Image src={src}/>
            </div>
        )
    }
}

Logo.defaultProps = {
    /**
     * @type img src of log
     */
    src: "/img/logo.png"
}

Logo.propTypes = {}

export default Logo
