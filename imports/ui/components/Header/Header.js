import React from 'react';
import {Component} from 'react'
import './header.css'
import {Image, Segment, Grid} from 'semantic-ui-react'

import Search from './Search'
import SignMenu from './SignMenu'
import {FormattedHTMLMessage} from "react-intl";
import ScrAmi from '../Util/ScrAmi'
/**
 *Header of Main Layout
 */

export class Header extends Component {
    fixHeader=()=> {
        if (window.scrollY >= 795) {
            /**
             * todo make brench marks for asking first  && !this.banner.className.classList.contains("fixed-header") and the adding or just adding like now
             */
            this.nav.classList.add("fixed-header")
        } else {
            this.nav.classList.remove("fixed-header")
        }
    }

    componentDidMount() {

        console.log('this.nodethis.nodethis.node', this.node)
        this.scrAmi = new ScrAmi(this.node)
        this.scrAmi.create()
        window.addEventListener('scroll', this.fixHeader)
    }

    componentWillUnmount() {
        this.scrAmi.destroy()
        window.removeEventListener('scroll', this.fixHeader)
    }

    render() {
        const goToEnd = () => {
            window.scrollTo(700)
        }
        const {search, signMenu} = this.props
        return (
            <div ref={node => this.node = node}>
                <div id="header">
                    <div className="cloud1 small cloud-speed2" data-scrami-inc="250" data-scrami-end="700"><img src="images/header/5-cloud.png" alt=""/></div>
                    <div className="cloud2 cloud-speed1" data-scrami-inc="250" data-scrami-end="700"><img src="images/header/5-cloud.png" alt=""/></div>
                    <div className="cloud3 cloud-speed3" data-scrami-inc="250" data-scrami-end="700" data-scrami-from="0"><img src="images/header/5-cloud.png" alt=""/></div>
                    <div className="cloud4 cloud-speed4" data-scrami-inc="250" data-scrami-end="700"><img src="images/header/2-cloud.png" alt=""/></div>
                    <div className="cloud7 cloud-speed2" data-scrami-inc="250" data-scrami-end="700"><img src="images/header/5-cloud.png" alt=""/></div>
                    <div className="brite" data-scrami-inc="-150" data-scrami-end="300"><img src="images/header/brite.png" alt=""/></div>
                    <div className="sun" data-scrami-inc="-150" data-scrami-end="300"><img src="images/header/sun.png" alt=""/></div>
                    <div className="main-mountain" data-scrami-inc="240" data-scrami-end="700" data-ratio="-0.4"><img src="images/header/main-mountain.png" alt=""/></div>
                    <div className="mountain" data-scrami-inc="200" data-scrami-end="700"><img src="images/header/mountain.png" alt=""/></div>
                    <div className="left-mountain" data-scrami-inc="200" data-scrami-end="700"><img src="images/header/mountain.png" alt=""/></div>
                    <div className="logo" data-scrami-inc="200" data-scrami-end="410"><img src="images/header/logo.png" alt=""/></div>
                    <div className="logotype" data-scrami-inc="260" data-scrami-end="450"><img src="images/header/logotype.png" alt=""/></div>
                    <div className="knoll4" data-scrami-inc="160" data-scrami-end="700"><img src="images/header/knoll4.png" alt=""/></div>
                    <div className="right-mountain" data-scrami-inc="160" data-scrami-end="700"><img src="images/header/mountain.png" alt=""/></div>
                    <div className="knoll3" data-scrami-inc="120" data-scrami-end="700"><img src="images/header/knoll3.png" alt=""/></div>
                    <div className="knoll2" data-scrami-inc="80" data-scrami-end="700"><img src="images/header/knoll2.png" alt=""/></div>
                    <div className="knoll" data-scrami-inc="40" data-scrami-end="700"><img src="images/header/knoll1.png" alt=""/></div>
                    <div className="tree" data-scrami-inc="40" data-scrami-end="700"><img src="images/header/tree.png" alt=""/></div>
                    <div className="shrubbery" data-scrami-inc="40" data-scrami-end="700"><img src="images/header/shrubbery.png" alt=""/></div>
                    <div className="banner-heading" data-scrami-inc="400" data-scrami-end="630">
                        <h1>
                            <FormattedHTMLMessage
                                id="Header.Header.banner"
                                defaultMessage='We’re<br/>Creating a<br/> <span style="font-weight: 900;color: #46b857 ;">Brighter</span><br/> <span style="font-weight: 900;color: #f7a449;">World</span><br/>'
                            />

                        </h1>
                    </div>
                    <div className="slogan" data-scrami-property="opacity" data-scrami-inc="-1" data-scrami-end="250">
                        <FormattedHTMLMessage
                            id="Header.Header.slogan"
                            defaultMessage='<b>Our Intention</b>: Empower one-another with ample
 currency and opportunity to live well with freewill '
                        />
                        <div className="arrows" onClick={goToEnd}>
                            <svg>
                                <path className="a1" d="M0 0 L30 32 L60 0"/>
                                <path className="a2" d="M0 20 L30 52 L60 20"/>
                                <path className="a3" d="M0 40 L30 72 L60 40"/>
                            </svg>
                        </div>
                    </div>
                </div>
                <div className="after-banner" ref={nav => this.nav = nav}>
                    <Image src="/images/header/blue-river.svg" className="blue-river"/>
                    <Grid stackable>
                        <Grid.Row centered verticalAlign="middle">
                            <Grid.Column tablet="1" computer="4" /*only="tablet computer large screen widescreen"*/>
                                <Image src="images/header/logo.png" className="nav-logo" data-scrami-property="opacity" data-scrami-inc="1" data-scrami-start="550" data-scrami-end="700"/>
                            </Grid.Column>

                            <Grid.Column tablet="10" computer="8">
                            </Grid.Column>

                            <Grid.Column tablet="5" computer="4">
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
            </div>

        )
    }
}
/**
 *
 * @type {{leftContent: XML, centerContent: XML, rightContent: XML, leftWidth: number, centerWidth: number, rightWidth: number}}
 */
Header.defaultProps = {
    /**
     * @type {Component} Center content of header
     */
    search: <Search />,
    /**
     * @type {Component} Right content of header
     */
    signMenu: <SignMenu />,
}

export default Header