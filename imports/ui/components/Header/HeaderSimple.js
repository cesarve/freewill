import React, {Component} from 'react';
import PropTypes from 'prop-types'
import './headersimple.less'
import {Grid, Image} from "semantic-ui-react";

class HeaderSimple extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <div id="header-simple">
                    <Grid stackable>
                        <Grid.Row centered verticalAlign="middle">
                            <Grid.Column tablet="1" computer="4" /*only="tablet computer large screen widescreen"*/>
                                <Image src="images/header/logo.png" className="nav-logo" data-scrami-property="opacity" data-scrami-inc="1" data-scrami-start="550" data-scrami-end="700"/>
                            </Grid.Column>

                            <Grid.Column tablet="10" computer="8">
                            </Grid.Column>

                            <Grid.Column tablet="5" computer="4">
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </div>
                <div id="header-simple-padder"></div>
            </div>
        )
    }

    static defaultProps = {}

    static propTypes = {}
}


export default HeaderSimple
