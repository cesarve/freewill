/**
 * Auth Menu (Sign In|Sign up) for header in Main Layout
 */
import React from 'react';
import {Component} from 'react'
import PropTypes from 'prop-types'
import {Icon, Grid} from 'semantic-ui-react'
import {Link, withRouter} from 'react-router-dom'
import {Dropdown} from "semantic-ui-react/";
import {withTracker} from 'meteor/react-meteor-data'


class SignMenu extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const {userId, handleLogout} = this.props
        if (!userId) return (
            <Grid columns={2} verticalAlign='middle' centered>
                <Grid.Row columns={2}>
                    <Grid.Column textAlign='center'>
                        <Link to="/sign-up" className="inverted link">
                            <Icon name='add user'/>
                            Create a account
                        </Link>
                    </Grid.Column>
                    <Grid.Column textAlign='center'>
                        <Link to="/sign-in" className="inverted link">
                            <Icon name='sign in'/>
                            Sign in
                        </Link>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )

        if (userId) return (
        <Grid columns={2} verticalAlign='middle' centered>
            <Grid.Row columns={2}>
                <Grid.Column textAlign='center' floated="right">
                    <Dropdown text='My account' inline icon='user'>
                        <Dropdown.Menu>
                            <Dropdown.Item>
                                Edit profile
                            </Dropdown.Item>
                            <Dropdown.Divider />
                            <Dropdown.Item as={Link} to='/sign-in' onClick={handleLogout}>
                                Logout
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Grid.Column>
            </Grid.Row>
        </Grid>
        )

    }

    static defaultProps = {}

    static propTypes = {}

}


export default withTracker(({history}) => {
    return {
        userId: Meteor.userId(),
        user: Meteor.user(),
        handleLogout: Meteor.logout,
    }
})
(SignMenu);




