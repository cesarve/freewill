import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {FormattedHTMLMessage} from "react-intl";
import { Segment} from 'semantic-ui-react'


class Mission extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Segment clearing basic textAlign="center" className="margin-top-3 margin-bottom-3" >
                    <FormattedHTMLMessage
                        id={ 'Home.Mission.text' }
                        defaultMessage={ `<h3>We're  creating a world where we can all live well with <span className="text blue ">freewill</span>, by selling some of our products and services for <strong
                class="text green">power</strong></h3>` }
                    />
            </Segment>

    )
    }

    static defaultProps = {}

    static propTypes = {}
    }


    export default Mission
