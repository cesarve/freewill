import React, {Component} from 'react';
import { FormattedHTMLMessage} from "react-intl";
import {Grid, Icon, Segment} from 'semantic-ui-react'
import './how.less'

class How extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Segment clearing basic className="how gradient-animated">
                <Grid stackable centered relaxed  >
                    <Grid.Row columns={3} >
                        <Grid.Column  textAlign="center"  verticalAlign="middle"  >
                            <Icon name="circle check" color="green" size="big"/><br/><br/>
                            <FormattedHTMLMessage
                                id={ 'Home.How.first_column' }
                                defaultMessage={`We gain 420 <strong class="text green">power</strong>  by creating an account`}
                            />

                        </Grid.Column>
                        <Grid.Column  textAlign="center" verticalAlign="middle" >
                            <Icon name="circle check" color="green" size="big"/><br/><br/>
                            <FormattedHTMLMessage
                                id={ 'Home.How.second_column' }
                                defaultMessage={`We gain 105 more <strong class="text green">power</strong> every Sunday`}
                            />

                        </Grid.Column >
                        <Grid.Column  textAlign="center" verticalAlign="middle" >
                            <Icon name="circle check" color="green" size="big"/><br/><br/>
                            <FormattedHTMLMessage
                                id={ 'Home.How.third_column' }
                                defaultMessage={`We gain more <strong class="text green">power</strong>  by selling products and services`}
                            />

                        </Grid.Column >
                        <Grid.Column textAlign="center" verticalAlign="middle" >
                            <Icon name="circle check" color="green" size="big"/><br/><br/>
                            <FormattedHTMLMessage
                                id={ 'Home.How.fourth_column' }
                                defaultMessage={`We use our <strong class="text green">power</strong> to purchase products and services`}
                            />

                        </Grid.Column>
                    </Grid.Row>
                </Grid>

            </Segment>

        )
    }

    static defaultProps = {}

    static propTypes = {}
}


export default How
