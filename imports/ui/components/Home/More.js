import React, {Component} from 'react';
import {FormattedHTMLMessage} from "react-intl";
import {Grid, Header,  Image, Segment} from 'semantic-ui-react'
class More extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Segment clearing basic className="more margin-top-5 margin-bottom-5">
                <Grid stackable centered relaxed>
                    <Header as="h3">
                        <FormattedHTMLMessage
                            id={ 'Home.How.title' }
                            defaultMessage={'We also user freewill to grow, gain more credibility and to promote'}
                        />
                    </Header>

                    <Grid.Row columns={3}>
                        <Grid.Column textAlign="center" verticalAlign="middle">
                            <Image src="/icons/medal.svg" centered style={{width: '90px'}}/><br/>
                            <FormattedHTMLMessage
                                id={ 'Home.How.first_column' }
                                defaultMessage={'We can live and grow more genuinely <br/> by providing products and services as we want to'}
                            />

                        </Grid.Column>
                        <Grid.Column textAlign="center" verticalAlign="middle">
                            <Image src="/icons/like.svg" centered style={{width: '90px'}}/><br/>
                            <FormattedHTMLMessage
                                id={ 'Home.How.second_column' }
                                defaultMessage={'We can gain more clarity and credibility<br/>  by welcoming reviews for our products and services'}
                            />

                        </Grid.Column >
                        <Grid.Column textAlign="center" verticalAlign="middle">
                            <Image src="/images/home/websight-logo.png" centered style={{width: '90px', backGraundColor: '#EEEEEE'}} shape="circular"/><br/>
                            <FormattedHTMLMessage
                                id={ 'Home.How.third_column' }
                                defaultMessage={'We can promote our other websites <br/> by hyperlinking our product and services to them'}
                            />

                        </Grid.Column >
                    </Grid.Row>
                </Grid>

            </Segment>

        )
    }

    static defaultProps = {}

    static propTypes = {}
}


export default More
