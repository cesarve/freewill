/**
 * Created by cesar on 25/9/17.
 */
/**
 * Created by Jose Carmona on 20/09/17.
 */

import {Header, Label, Button, Segment, Checkbox, Form, Input, Select, TextArea, Grid, Accordion, Icon} from 'semantic-ui-react'
import React from 'react';
import {Component} from 'react'
import AutoForm from  "../../uniforms/AutoForm";
import TextField from 'uniforms-react-semantic/TextField'
import ErrorsField from 'uniforms-react-semantic/ErrorsField'
import LongTextField from 'uniforms-react-semantic/LongTextField'
import BoolField from 'uniforms-react-semantic/BoolField'
import {FormattedMessage} from 'react-intl';
import {contactSchema} from './../../../api/Contact/Contact'
import {withTracker} from 'meteor/react-meteor-data'
import {toPromise} from "../../../api/util";
import './contact.less'

/**
 * Render Sign up Form
 */

export class Contact extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        const {onSubmit} = this.props
        return (
            <div>

                <Segment basic color="orange" inverted className="footer-contact">
                    <AutoForm schema={contactSchema} onSubmit={onSubmit} model={{}} showInlineError>
                        <Header as='h2' textAlign='center'>
                            <FormattedMessage
                                id={ 'Home.Contact.header' }
                                defaultMessage={ 'Contact' }
                            />
                        </Header>
                        <Grid columns='equal'>
                            <Grid.Row >
                                <Grid.Column tablet={16} computer={8}>
                                    <Form.Field >
                                        <TextField /*placeholder='name'*/ name='name'/>
                                    </Form.Field >
                                </Grid.Column>
                                <Grid.Column tablet={16} computer={8}>
                                    <Form.Field >
                                        <TextField /*placeholder='email'*/ name='email'/>
                                    </Form.Field >
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row>
                                <Grid.Column width={16}>
                                    <Form.Field >
                                        <LongTextField icon='file text outline'
                                                       autoHeight
                                            /*placeholder='message'*/
                                                       name='message'/>
                                    </Form.Field>
                                </Grid.Column>
                                <Grid.Column width={16}>
                                    <BoolField name='sendCopy'/>
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row textAlign='center'>
                                <Grid.Column >
                                    <Button color='orange' fluid inverted>Send</Button>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>

                        <ErrorsField  />
                    </AutoForm>
                </Segment>
            </div>
        )
    }
}


export default withTracker(({history}) => {
    return ({
        onSubmit(model) {
            model = contactSchema.clean(model)
            return toPromise(Meteor.call, 'contactSendEmail', model);
        }
    })
})(Contact);