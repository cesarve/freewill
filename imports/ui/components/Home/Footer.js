/**
 * Created by cesar on 6/10/17.
 */
import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {FormattedHTMLMessage} from "react-intl";
import {Divider, Grid, Icon, Segment} from 'semantic-ui-react'
import Contact from "./Contact";
import {Link} from "react-router-dom";
import './footer.less'
class Footer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Segment basic color="orange" inverted>
                <Grid stackable>
                    <Grid.Row columns={3}>
                        <Grid.Column>
                        </Grid.Column>

                        <Grid.Column>
                        </Grid.Column>

                        <Grid.Column>
                            <Contact/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <Divider inverted  clearing fitted/>
                <Link to="https://www.facebook.com/FreewillUnlimited" target="_blank">
                    <Icon name="facebook square" inverted size="huge"/>
                </Link>
            </Segment>

        )
    }

    static defaultProps = {}

    static propTypes = {}
}


export default Footer
