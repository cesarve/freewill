/**
 * Created by Jose Carmona on 11/10/17.
 */
import SimpleSchema  from 'simpl-schema';

export const schema = new SimpleSchema({
	present:{
		type: String,
	},
	relevantTo:{
		type: String,
	},
	type:{
		type: String,
	},
	arrangeBy:{
		type: String,
		label: "Arrage By "
	}
});