/**
 * Created by Jose Carmona on 08/10/17.
 */
import SimpleSchema  from 'simpl-schema';

export const schema = new SimpleSchema({
	present:{
		type: String,
	},
	availableIn:{
		type: String,
	},
	competency:{
		type: String,
	},
	shareBy:{
		type: String,
	},
	gender:{
		type: String,
	},
	age:{
		type: String,
	},
	"age.from":{
		type: Number,
	},
	"age.to":{
		type: Number,
	},
	provided:{
		type: String,
	},
	"provided.on":{
		type: String,
	},
	"provided.at":{
		type: String,
	},
	arrangeBy:{
		type: String,
		label: "Arrage By "
	}
});