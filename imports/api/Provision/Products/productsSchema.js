/**
 * Created by Jose Carmona on 10/10/17.
 */
import SimpleSchema  from 'simpl-schema';

export const schema = new SimpleSchema({
	present:{
		type: String,
	},
	availableIn:{
		type: String,
	},
	history:{
		type: String,
	},
	provided:{
		type: String,
	},
	withFree:{
		type: String,
	},
	status:{
		type: String,
	},
	arrangeBy:{
		type: String,
		label: "Arrage By "
	}
});