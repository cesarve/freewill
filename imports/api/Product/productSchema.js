/**
 * Created by Jose Carmona on 05/10/17.
 */

import SimpleSchema  from 'simpl-schema';
import { FormattedMessage } from 'react-intl';
import React from 'react'

export const schema = new SimpleSchema({
    shareTime:{
        type: Date,
        defaultValue: new Date(),
        label:  <FormattedMessage id={ "Products.Product.shareTime" } defaultMessage={ "Share Time "  } />,
    },
    from: {
        type: Date,
        defaultValue: new Date(),
        label: <FormattedMessage id={ "Products.Product.from" } defaultMessage={ "From "  } />,
    },
    to: {
        type: Date,
        defaultValue: new Date(),   
        label: <FormattedMessage id={ "Products.Product.to" } defaultMessage={ "To "  } />, 	 
    },
    notice: {
    	type: Object,
    },
    "notice.Required": {
 		type: String,
 		label: <FormattedMessage id={ "Products.Product.requiered" } defaultMessage={ "Requiered "  } />,
    },
    "notice.Hours": {
    	type: String, 
    	label: <FormattedMessage id={ "Products.Product.hours" } defaultMessage={ "Hours "  } />,  	 
    },   
    requestFor: {
    	 type: String,
    	 label: <FormattedMessage id={ "Products.Product.requestFor" } defaultMessage={ "Request For "  } />,
    },
    message: {
	    type: String,
	    min: 5,
	    label: <FormattedMessage id={ "Products.Product.message" } defaultMessage={ "Message "  } />,
    }
})
