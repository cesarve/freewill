import {Random} from 'meteor/random'
import {Roles} from 'meteor/alanning:roles'
/**
 * Email validator
 * @param {string} email to be validated
 * @returns {boolean}
 */
export const isValidEmail = function (email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

/**
 * Check if a user is in role
 * @param {Object|String} user -  Either or user object or user Id
 * @param {String|Array} role
 * @returns {Boolean}
 */
export const is = (user, role) => {
    if (typeof user === 'string') {
        return Roles.userIsInRole(user, role)
    } else {
        return user && user.roles && Array.isArray(user.roles) && user.roles.indexOf(role) >= 0
    }
}


/**
 * Check if a user is in admin
 * @param {Object|String} user -  Either or user object or user Id
 * @returns {Boolean}
 */
export const isAdmin = (user) => {
    return is(user, 'admin')
}

export const isUser = (user) => {
    return user.profile.status !== 'Suspended' && is(user, 'user')
}


/**
 * Generate random numbers
 * @param {number} n digits length
 * @return {string}
 */
export const generateCode = (n = 5) => (Random.fraction() * 100000000).toString().replace('.', '').substring(0, n);


/**
 * Standardize a phone to a well formatted international phone number
 * @param {number|string} prefix - contry code
 * @param {number|string} number - phone number
 * @return {string}
 */
export const standardizePhone = function ({prefix, number}) {
    //TODO IMPORTANT
    return '+' + prefix + number
    /*const PhoneNumber = require('google-libphonenumber')
     const PhoneNumberFormat = require('google-libphonenumber')
     const PhoneNumberUtil = require('google-libphonenumber')
     prefix = parseInt(prefix.replace(/\D/g, ''));
     number = parseInt(number.replace(/\D/g, ''));
     const phoneUtil = PhoneNumberUtil.getInstance();
     const phoneNumber = new PhoneNumber().setCountryCode(prefix).setNationalNumber(number)
     return phoneUtil.format(phoneNumber, PhoneNumberFormat.E164);*/
}

export const toPromise = function (fnc, ...args) {
    return new Promise((resolve, reject) => {
        console.log('Promise', ...args)
        fnc(...args, (err, result) => {
            if (err) {
                reject(err)
                return
            }
            resolve(result)
        })
    })
}


export const checkUserId = function () {
    if (!this.userId) throw new Meteor.Error(403, 'Access denied')
}

export const checkAdmin = function () {
    if (!this.userId || !Roles.usersIsRole(this.userId, 'admin')) throw new Meteor.Error(403, 'Access denied')
}


export const schemaMixin = function (methodOptions) {
    methodOptions.validate = methodOptions.schema.validator({clean: true});
    return methodOptions;
}

export const promiseMixin = function (methodOptions) {
    methodOptions.promise = function promise(args) {
        return new Promise((resolve, reject) => {
            Meteor.call(methodOptions.name, args, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }
    return methodOptions;
}