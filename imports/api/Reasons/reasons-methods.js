/**
 * Created by cesar on 5/12/17.
 */


import {ValidatedMethod} from 'meteor/mdg:validated-method'
import {isAdmin,schemaMixin} from "../util";
import Reasons from '/imports/api/Reasons/Reasons'
import SimpleSchema from 'simpl-schema'

export const insertReason = new ValidatedMethod({
    name: 'insertReason',
    mixins: [schemaMixin],
    schema: new SimpleSchema({reason: String}), // argument validation
    run({reason}){
        if (!isAdmin(this.userId)) throw new Meteor.Error(403,'Access denied!')
        return Reasons.upsert({reason},{reason})
    }
});


