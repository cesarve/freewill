/**
 * Created by cesar on 5/12/17.
 */
import {isAdmin} from "../../util";
import Reasons from '/imports/api/Reasons/Reasons';

Meteor.publish('reasons', function () {
    if (!isAdmin(this.userId)) return this.ready()
    return Reasons.find()
})