/**
 * Created by cesar on 22/9/17.
 */
import SimpleSchema from 'simpl-schema'

export const userSignUpSchema = new SimpleSchema({
    firstName: {type: String},
    familyName: {type: String},
    email: {type: String, regEx: SimpleSchema.RegEx.Email},
    password: {type: String},
    terms: {type: Boolean},
})


export const userProfileSchema = new SimpleSchema({
    "avatar": {type: Object, optional: true,},
    "avatar.url": {type: String},
    //"avatar.status": {type: String},
    "firstName": {type: String},
    "familyName": {type: String},
    "description": {type: String, optional: true,},
    "location": {type: Object},
    "location.place_id": {type: String},
    "location.description": {type: String},
    "location.id": {type: String},
    "phone": {type: Object},
    "phone.prefix": {type: String},
    "phone.number": {type: String},
    "birthday": {type: Date},
    "gender": {type: String},
    "shareWith": {type: Object},
    "shareWith.familyName": {type: String, defaultValue: 'everybody'},
    "shareWith.phone": {type: String, defaultValue: 'everybody'},
    "shareWith.location": {type: String, defaultValue: 'everybody'},
    // "shareWith.birthday": {type: Boolean}, defaultValue: 'everyBody',
})


export const userVerifySchema = new SimpleSchema({
    email: {type: String, regEx: SimpleSchema.RegEx.Email, label: 'Email address'},  // special label, this works forMy Custom Autoform which handle errors from server by matchin labels with errors
    emailCode: {type: String},
    emailVerified: {type: Boolean, optional: true},
    "phone": {type: Object, label: 'PHONE'}, // special label, this works forMy Custom Autoform which handle errors from server by matchin labels with errors
    "phone.prefix": {type: String},
    "phone.number": {type: String, label: 'Phone number'},  // special label, this works forMy Custom Autoform which handle errors from server by matchin labels with errors
    "phoneVerified": {type: Boolean, optional: true},
    phoneCode: {type: String},

})


export const userSignInSchema = new SimpleSchema({
    email: {type: String, regEx: SimpleSchema.RegEx.Email, label: 'User'},
    password: {type: String},
})


export const userSchema = new SimpleSchema({
    emails: {
        type: Array,
        optional: true
    },
    "emails.$": {
        type: Object
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date
    },
    profile: {
        type: userProfileSchema,
        optional: true
    },
    roles: {
        type: Array,
        optional: true
    },
    'roles.$': {
        type: String
    },
});
