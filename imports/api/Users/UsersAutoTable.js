/**
 1) ID (creator's account ID)
 2) Image (creator's main image)
 3) Name (creator's name)
 4) Location (their current location)
 5) Brief (their introduction text)
 6) Joined (date)
 7) Recently live (date)
 8) Status  (If status = suspended, enable clicking this to view a popup showing when the suspension will end)
 9) Substatus  (Lively or Dormant)
 11) Abilities   (#, of abilities identified)
 12) Services (#, of abilities identified)
 12) Time sold  (hrs)
 13) Time gifted (hrs)
 14) Products sold  (#, of products sold)
 15) Products gifted (#, of products shared freely)
 16) Ability reviews (#, of reviews received)
 17) Service reviews (#, or reviews received)
 18)Product reviews (#, of reviews received)
 19) Power  (#, total power balance)
 20) Shares  (#, of shared they're holding)
 21) Email  (verified email of account)
 22) Mobile (verified mobile of account)
 23) Invited by (name of creator who invited them)
 24) Invites accepted (# of invites sent and accepted)
 25) Subscribed (whether subscribed for newsletter)
 26) Action  (Show an "Edit" action button)
 */
import createAutoTable from 'meteor/cesarve:react-auto-table'
import {userSchema as schema} from './Users'
import {CellDate, CellBoolean} from "meteor/cesarve:react-auto-table/util";

let UserActions, UserActionAvatar, Dropdown, React

if (Meteor.isClient) {
    React = require('react')
    UserActions = require('/imports/ui/components/Admin/Users/UserActions').default
    UserActionAvatar = require('/imports/ui/components/Admin/Users/UserActionAvatar').default
    Dropdown = require("semantic-ui-react").Dropdown
}

const boolean = {
    filterMethod: ({id, value}) => value === '' ? null : ({[id]: value === 'Yes' ? true : {$ne: true}}),
    Filter: ({filter = {}, onChange}) => {
        const options = [
            {key: '', text: '', value: ''},
            {key: 'Yes', text: 'Yes', value: 'Yes'},
            {key: 'No', text: 'No', value: 'No'},
        ]
        return Meteor.isClient && (
                <div>
                    <Dropdown
                        selection
                        fluid
                        options={options}
                        onChange={(e, {value}) => onChange(value)}/>
                </div>)
    }
}

const Collection = Meteor.users
const columns = [
    {
        Header: 'ID',
        accessor: '_id'
    },
    {
        Header: 'Avatar',
        accessor: 'profile.avatar',
        Cell: UserActionAvatar,
        headerClassName: 'no-hide',
        filterMethod: ({value}) => Array.isArray(value) && value.length && {"profile.avatar.status": {$in: value}},
        Filter: ({filter = {}, onChange}) => {
            const options = [
                {key: 'Pending', text: 'Pending', value: 'Pending'},
                {key: 'Approved', text: 'Approved', value: 'Approved'},
                {key: 'Declined', text: 'Declined', value: 'Declined'},
            ]
            return Meteor.isClient && <div>
                    <Dropdown placeholder='Status' fluid multiple selection options={options} onChange={(e, {value}) => onChange(value)}/>
                </div>
        },
    },
    {
        Header: 'Location',
        accessor: 'profile.location.description',
    },
    {
        Header: 'Brief',
        accessor: 'profile.description',
    },
    {
        Header: 'Joined',
        accessor: 'createdAt',
        Cell: CellDate
    },
    {
        Header: 'Recently live',
        accessor: 'profile.lastLogin',
        Cell: CellDate,

    },
    {
        Header: 'Status',
        accessor: 'profile.status',
        headerClassName: 'no-hide',
        filterMethod: ({value}) => value && ({"profile.status": value}),
        Filter: ({filter = {}, onChange}) => {
            const options = [
                {key: '', text: ''},
                {key: 'Enabled', text: 'Enabled', value: 'Enabled'},
                {key: 'Suspended', text: 'Suspended', value: 'Suspended'},
            ]
            return Meteor.isClient && (
                    <div>
                        <Dropdown
                            selection
                            fluid
                            options={options}
                            onChange={(e, {value}) => onChange(value)}/>
                    </div>)
        }
        ,
    },
    {
        Header: 'Online',
        accessor: 'profile.online',
        Cell: CellBoolean,
        filterMethod: boolean.filterMethod,
        Filter: boolean.Filter,
        headerClassName: 'no-hide',
    },
    {
        Header: 'Substatus',
        accessor: 'subtatus',
    },
    {
        Header: 'Abilities',
        accessor: 'abilitiesCount',
    },
    {
        Header: 'Time sold ',
        accessor: 'timeSold',
    },
    {
        Header: 'Time gifted',
        accessor: 'profile.timeGifted',
    },
    {
        Header: 'Products sold',
        accessor: 'productsSoldCount',
    },
    {
        Header: 'Products gifted ',
        accessor: 'productsGiftedCount',
    },
    {
        Header: 'Ability reviews',
        accessor: 'abilityReviewsCount',
    },
    {
        Header: 'Service reviews',
        accessor: 'serviceReviewsCount',
    },
    {
        Header: 'Product reviews',
        accessor: 'productReviews',
    },
    {
        Header: 'Services',
        accessor: 'abilitiesServices',
    },
    {
        Header: 'Power',
        accessor: 'power',
    },

    {
        Header: 'Shares',
        accessor: 'sharesCount',
    },
    {
        Header: 'Mobile',
        accessor: 'profile.familyName',
    },

    {
        Header: 'Email',
        accessor: 'emails.0.address'
    },
    {
        Header: 'Invited by',
        accessor: 'invitedBy.name' //todo when profiles names is update update all inveted profiles
    },
    {
        Header: 'Invites accepted',
        accessor: 'invites.acceptedCount'
    },
    {
        Header: 'Subscribed',
        accessor: 'subscribed',
        Cell: CellBoolean,
        filterMethod: boolean.filterMethod,
        Filter: boolean.Filter,
        headerClassName: 'no-hide',

    },
    {
        Header: 'Actions',
        accessor: '_id',
        Cell: UserActions,
        filterable: false,
    }
]
const publish = function (props) {
    return Roles.userIsInRole(this.userId, 'admin')
}

export default createAutoTable({Collection, publish, schema, columns})
