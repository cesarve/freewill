/**
 * Created by cesar on 22/9/17.
 */
import {isAdmin} from "../../util";
Meteor.publish('me', function () {
    if (!this.userId) return this.ready()
    return Meteor.users.find(this.userId, {fields: {profile: 1}})
})
Meteor.publish('user', function (userId) {
    if (!this.userId || !isAdmin(this.userId)) return this.ready()
    return Meteor.users.find(userId, {fields: {profile: 1}})
})