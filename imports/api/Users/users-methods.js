/**
 * Created by cesar on 22/9/17.
 */
import {ValidatedMethod} from 'meteor/mdg:validated-method'
import {userProfileSchema, userSignUpSchema, userVerifySchema} from '/imports/api/Users/Users'
import SimpleSchema from 'simpl-schema'


import {generateCode, standardizePhone, checkUserId, schemaMixin, promiseMixin, isAdmin} from "../util";

let twilio, React

if (Meteor.isClient) {
    React = require('react')
} else {
    twilio = require('twilio').default
}

export const userUpdateProfile = new ValidatedMethod({
    name: 'userUpdateProfile',
    mixins: [schemaMixin, promiseMixin],
    schema: userProfileSchema, // argument validation
    run(profile){
        console.log('profileprofile', profile)
        checkUserId.call(this)
        return Meteor.users.update(this.userId, {$set: {profile}})
    }
});

export const userUpdateStatus = new ValidatedMethod({
    name: 'userUpdateStatus',
    mixins: [schemaMixin, promiseMixin],
    schema: new SimpleSchema({
        userId: String,
        status: String,
        reason: {type: String, optional: true}
    }),
    run({userId, status, reason}){
        console.log('status', status)
        console.log('reason2', reason)
        console.log('userId', userId)

        if (!isAdmin(this.userId)) throw new Meteor.Error(403, 'Access denied')
        if (userId === this.userId) throw new Meteor.Error(400, "You can't update yourself")
        Meteor.users.update(userId, {$set: {"profile.status": status, "profile.reason": reason}})
        console.log(Meteor.users.findOne(userId))
        return true
    }
});


export const userAvatarUpdateStatus = new ValidatedMethod({
    name: 'userAvatarUpdateStatus',
    mixins: [schemaMixin, promiseMixin],
    schema: new SimpleSchema({
        userId: String,
        status: {type: String, allowedValues: ['Declined', 'Approved']}
    }), // argument validation
    run({status, userId}){
        //TODO
        //checkAdmin.call(this)
        //if (userId === this.userId) throw new Meteor.Error(400, "You can't suspend yourself")
        return Meteor.users.update(userId, {$set: {"profile.avatar.status": status}})
    }
});


export const userAccountCreate = new ValidatedMethod({
    name: 'userAccountCreate',
    mixins: [schemaMixin, promiseMixin],
    schema: userSignUpSchema, // argument validation
    run({email, password, firstName, familyName}){
        console.log('email, password, firstName, familyName', email, password, firstName, familyName)
        return Accounts.createUser({
            email: email,
            password: password,
            profile: {
                firstName: firstName,
                familyName: familyName,
                status: 'Pending'
            }
        })
    }
});

export const userResendEmailCode = new ValidatedMethod({
    name: 'userResendEmailCode',
    validate: new SimpleSchema({
        email: String,
    }).validator(), // argument validation
    run({email}){
        checkUserId.call(this)
        if (Meteor.users.find({_id: {$ne: this.userId}, "emails.address": email}).count()) {
            throw new Meteor.Error(403, 'Email already exists.')
        }
        let token = generateCode()
        const tokenRecord = {
            token: token,
            address: email,
            when: new Date(),
        }
        Meteor.users.update({_id: this.userId}, {$set: {"emails.0.address": email}, $push: {'services.email.verificationTokens': tokenRecord}})
        const confirmUrl = `${Meteor.absoluteUrl()}/verifying-email/${this.userId}/${token}`
        //todo send email
        console.log(confirmUrl)

    }
});
export const userResendPhoneCode = new ValidatedMethod({
    name: 'userResendPhoneCode',
    validate: new SimpleSchema({
        phone: Object,
        "phone.prefix": String,
        "phone.number": String,
    }).validator(), // argument validation
    run({phone}){
        checkUserId.call(this)
        let token = generateCode(4)
        const tokenRecord = {
            token: token,
            phone: phone,
            when: new Date(),
        };
        Meteor.users.update(this.userId, {$set: {"profile.phone": phone}, $push: {'services.phone.verificationTokens': tokenRecord}})
        const client = twilio(Meteor.settings.twilio.accountSid, Meteor.settings.twilio.authToken)
        return client.messages.create({
            body: `Your code for FreeWill is ${token}`,
            to: standardizePhone(phone),
            from: Meteor.settings.twilio.phone,
        })
            .then((result) => {
                console.log('result', result)
            })
            .catch((err) => {
                console.log('error', err)
                throw new Meteor.Error(400, err.message)
            })
    }
});

export const userVerifyEmail = new ValidatedMethod({
    name: 'userVerifyEmail',
    validate: new SimpleSchema({userId: String, emailCode: String}).validator(),
    run({userId, emailCode}){

        let user = Meteor.users.findOne(userId)
        if (!user) {
            throw  new Meteor.Error(404, 'Verification code invalid')
        }
        const email = user.emails && user.emails[0] && user.emails[0].address
        const emailVerified = Meteor.users.update({
                _id: this.userId,
                'services.email.verificationTokens': {$elemMatch: {token: emailCode, address: email}}
            },
            {
                $set: {"emails.0.verified": true, "emails.0.address": email},
                $unset: {"services.email.verificationTokens": 1}
            })

        if (!emailVerified) throw new Meteor.Error(404, 'Verification code invalid')


        if (user.profile && user.profile.phone && user.profile.phone.verified) {
            Meteor.users.update(user._id, {$set: {'profile.status': 'Enabled'}})
        }
        return true
    }
})

export const userVerify = new ValidatedMethod({
    name: 'userVerify',
    validate: userVerifySchema.validator(), // argument validation
    run({email, emailCode, phone: {prefix, number}, phoneCode}){
        checkUserId.call(this)
        const userEmail = Meteor.users.findOne({"emails.address": email})
        const errors = []

        if (!userEmail) {
            errors.push('Email address not found, please resend the code ')
        } else if (userEmail._id !== this.userId) {
            errors.push('Email address already exists. ')
        }
        const emailVerified = Meteor.users.update({
                _id: this.userId,
                'services.email.verificationTokens': {$elemMatch: {token: emailCode, address: email}}
            },
            {
                $set: {"emails.0.verified": true, "emails.0.address": email},
                $unset: {"services.email.verificationTokens": 1}
            })

        if (!emailVerified) errors.push('Invalid Email code ')


        const userPhone = Meteor.users.findOne({"profile.phone.number": number, "profile.phone.prefix": prefix})
        if (!userPhone) {
            errors.push('Phone number not found, please resend the code ')
        } else if (userPhone._id !== this.userId) {
            errors.push('Phone number already exists. ')
        }
        const phoneVerified = Meteor.users.update({
                _id: this.userId,
                'services.phone.verificationTokens': {$elemMatch: {token: phoneCode, phone: {prefix, number}}}
            },
            {
                $set: {"profile.phone": {verified: true, number, prefix}},
                $unset: {"services.phone.verificationTokens": 1}
            })
        console.log('phoneVerified', phoneVerified)

        if (!phoneVerified) errors.push('Invalid Phone code ')

        const user = Meteor.users.findOne(userEmail._id)
        if (user.emails && user.emails[0] && user.emails[0].verified) {
            Meteor.users.update(user._id, {$set: {'profile.status': 'Enabled'}})
        }
        if (errors.length) throw new Meteor.Error(404, errors)
        return true

    }
});

