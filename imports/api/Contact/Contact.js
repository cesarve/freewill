/**
 * Created by Jose Carmona on 05/10/17.
 */
import SimpleSchema  from 'simpl-schema';

export const contactSchema = new SimpleSchema({
    name: {
        type: String,
        min: 3,
    },
    email: {
        type: String,
        min: 5,
        regEx: SimpleSchema.RegEx.Email
    },
    sendCopy: {
        type: Boolean,
        optional: true,
    },
    message: {
        type: String,
        min: 5,
        label: 'Message*',
    }
});
