/**
 * Created by cesar on 27/9/17.
 */
import SimpleSchema from 'simpl-schema'
import {userProfileSchema} from "../../api/Users/Users";


Meteor.startup(() => {
    ServiceConfiguration.configurations.remove({
        service: "facebook"
    });


    ServiceConfiguration.configurations.insert({
        service: "facebook",
        appId: '120192588691338',
        secret: 'e3c25b20a0accc60d8997617e5e0ba41'
    });
})

Meteor.users.deny({update: () => true});
Accounts.validateLoginAttempt((login) => {
    if (login.allowed && login.user.profile && login.user.profile.status && login.user.profile.status === 'Suspended') {
        return new Meteor.Error(403, 'User account is suspended', login.user.profile.reason)
    }
    return true
})

Accounts.onCreateUser(function (options, user) {
    console.log('----->Accounts.onCreateUser', options, user)
    user.createdAt = new Date()
    user.profile = options.profile || {}
    user.profile.status = 'Enabled'
    if (user.services.facebook) {
        console.log('----->user.services.facebook', user.services.facebook)

        user.emails = [{address: user.services.facebook.email, verified: user.services.facebook.verified}];
        user.profile.firstName = user.services.facebook.first_name
        user.profile.familyName = user.services.facebook.last_name
        user.profile.gender = user.services.facebook.gender
        user.profile.location = user.services.facebook.locale
        user.profile.avatar = user.services.facebook.picture
    }


    return user;
});


Accounts.validateNewUser((user) => {
    console.log('Accounts.validateNewUser', user)
    new SimpleSchema({
        _id: {type: String},
        emails: {type: Array},
        'emails.$': {type: Object},
        'emails.$.address': {type: String},
        'emails.$.verified': {type: Boolean},
        services: {type: Object, blackbox: true},
        createdAt: {type: Date},
        profile: {type: Object},
        "profile.status": {type: String,  allowedValues: ['Enabled']},
        "profile.avatar": {type: Object, optional: true, blackbox: true},
        "profile.firstName": {type: String, optional: true,},
        "profile.familyName": {type: String, optional: true,},
        "profile.description": {type: String, optional: true,},
        "profile.location": {type: Object, optional: true,},
        "profile.location.place_id": {type: String, optional: true,},
        "profile.location.description": {type: String, optional: true,},
        "profile.location.id": {type: String, optional: true,},
        "profile.phone": {type: Object, optional: true,},
        "profile.phone.prefix": {type: String, optional: true,},
        "profile.phone.number": {type: String, optional: true,},
        "profile.birthday": {type: Date, optional: true,},
        "profile.gender": {type: String, optional: true,},
        "profile.shareWith": {type: Object, optional: true,},
        "profile.shareWith.familyName": {type: String, defaultValue: 'everybody', optional: true,},
        "profile.shareWith.phone": {type: String, defaultValue: 'everybody', optional: true,},
        "profile.shareWith.location": {type: String, defaultValue: 'everybody', optional: true,},
    }).validate(user);
    // Return true to allow user creation to proceed
    return true;
});
