/**
 * Created by cesar on 12/9/17.
 */
// Deny all client-side updates to user documents
Meteor.users.deny({
    update() { return true; }
});
