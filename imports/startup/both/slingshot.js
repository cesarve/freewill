/**
 * Created by cesar on 11/9/17.
 */
import {Random} from 'meteor/random'


Slingshot.fileRestrictions('avatar', {
    allowedFileTypes: ['image/png', 'image/jpeg', 'image/gif'],
    maxSize: parseInt(6291456),
});

// Misc
function createFilename(filename) {
    const extension = filename.split('.').slice(0).pop();
    return `${Random.id()}.${extension}`;
}

// Server code
if (Meteor.isServer) {
    Slingshot.createDirective('avatar', Slingshot.S3Storage, {
        bucket: Meteor.settings.AWSBucket,
        acl: 'public-read',
        authorize() {
            if (!this.userId) {
                throw new Meteor.Error(403, 'You must register a user first before uploading a file.');
            }
            return true;
        },
        key(file) {
            return `freewill/${this.userId}/avatar/${createFilename(file.name)}` ;
        },
    });
}